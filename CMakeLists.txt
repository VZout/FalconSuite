project(FalconSuite)

cmake_minimum_required(VERSION 3.0.2)

##### Compile Details ####
option(ARCH_WIN64 "Build for 64 bit." ON)
option(USE_DIRECTX12 "Build with dx12 impl." OFF)
option(USE_VULKAN "Build with vk impl." ON)

##### COMPILER SETTINGS #####
set (CMAKE_CXX_STANDARD 14)
message(STATUS "Using C++"  ${CMAKE_CXX_STANDARD})

##### OUTPUT DIRECTORIES #####
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

##### OPTIONS #####
option(FALCON_BUILD_TESTS "Build the Falcon Suite test programs" ON)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "Build the GLFW example programs")
set(GLFW_BUILD_TESTS OFF CACHE BOOL "Build the GLFW test programs")
set(GLFW_BUILD_DOCS OFF CACHE BOOL "Build the GLFW documentation")

##### DEPENDENCIES #####
add_subdirectory(deps/glfw3)
#add_subdirectory(deps/zlib)
add_subdirectory(deps/tinyobjloader)

##### DEFINITIONS #####
add_definitions(-DENABLE_VK_VALIDATION_LAYERS)
message(STATUS "FalconRenderer: Using Vulkan validation layers")

##### INCLUDE/LINK DIRECTORIES #####
# 64 BIT
if (ARCH_WIN64)
link_directories(C:/VulkanSDK/1.0.57.0/Lib
				${CMAKE_BINARY_DIR}/lib/
				${CMAKE_BINARY_DIR}/deps/glfw3/src
				${CMAKE_BINARY_DIR}/deps/zlib/)

else()
link_directories(C:/VulkanSDK/1.0.57.0/Lib32
				${CMAKE_BINARY_DIR}/lib/
				${CMAKE_BINARY_DIR}/deps/glfw3/src
				${CMAKE_BINARY_DIR}/deps/zlib/)

endif()

include_directories(
	C:/VulkanSDK/1.0.57.0/Include
	deps/glfw3/include
	deps/stb/
	deps/tinyobjloader
	modules/
)


##### MODULES/TESTS SUB-DIRECTORIES #####
add_subdirectory(modules)

if (FALCON_BUILD_TESTS)
	add_subdirectory(tests)
endif()
