#pragma once

//#define FR_USE_VULKAN
#define FR_USE_DX12

#include <chrono>
#include <cmath>
#include <renderer/interface.hpp>

unsigned int window_width = 600, window_height = 600;

#ifdef FR_USE_DX12
#define ADD_WINDOW_TITLE_SUFFIX  create_info.title += " [DirectX 12]";
#endif
#ifdef FR_USE_VULKAN
#define ADD_WINDOW_TITLE_SUFFIX  create_info.title += " [Vulkan]";
#endif

fr::Window window;
fr::Instance instance;
fr::RenderWindow render_window;
fr::Device device;
fr::CommandQueue queue;
fr::CommandList list;
fr::Fence fence[3];

fr::RootSignature root_signature;
fr::PipelineState ps;
fr::Viewport viewport;
fr::StagingBuffer vb;
fr::Shader s_vert;
fr::Shader s_frag;

// FPS Counter
namespace util {
	using namespace std::chrono;
	using hr_time_point = time_point<high_resolution_clock>;

	unsigned int frames;
	float time;
	const double pi = 3.141592653589793238463;

	hr_time_point last;

	void print_framerate() {
		auto now = high_resolution_clock::now();
		duration<double> diff = now - last;
		if (diff.count() >= 1) {
			std::cout << frames << "\n";
			frames = 0;
			last = now;
		}
		frames++;
	}

	void update_time() {
		static auto last = high_resolution_clock::now();
		auto now = high_resolution_clock::now();
		time = duration_cast<duration<double>>(now - last).count();
	}

	float sin_pulse(float t) {
		return 0.5 * (1 + sin(util::pi * util::time));
	}

	float cos_pulse(float t) {
		return 0.5 * (1 + cos(util::pi * util::time));
	}

} /* fps_c */