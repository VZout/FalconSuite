#include "game.hpp"

#include <renderer/vertex.hpp>

int main() {
	fr::WindowCreateInfo create_info;
	create_info.title = "Temporary test"; ADD_WINDOW_TITLE_SUFFIX
	create_info.fullscreen = false;
	create_info.width = window_width;
	create_info.height = window_height;
	fr::Create(window, create_info);

	/* INIT BACK-END */
	fr::Create(instance);
	fr::PreInitialize(render_window, instance, window);
	fr::Create(device, instance, render_window);
	fr::Create(queue, device, fr::CmdQueueType::DIRECT);

	std::vector<fr::RenderTargetAttachement> attachments;

	fr::Create(render_window, device, queue, window_width, window_height, attachments);

	for (auto i = 0; i < render_window.m_num_back_buffers; i++) {
		fr::Create(fence[i], device);
	}

	fr::Allocate(list, device, render_window.m_num_back_buffers);

	/* INIT FRONT-END */
	fr::Begin(list, render_window);

	fr::RootSignatureCreateInfo rs_create_info { 1 };
	fr::Create(root_signature, rs_create_info);
	fr::Finalize(root_signature, device);
	
	//fr::Load(s_vert, device, "temp_vert.spv");
	//fr::Load(s_frag, device, "temp_frag.spv");
	fr::Load(s_vert, device, fr::ShaderType::VERTEX_SHADER, "vertex.hlsl");
	fr::Load(s_frag, device, fr::ShaderType::PIXEL_SHADER, "pixel.hlsl");

	fr::SetVertexShader(ps, s_vert);
	fr::SetFragmentShader(ps, s_frag);
	fr::SetRootSignature(ps, &root_signature);
	fr::Finalize(ps, device, render_window);

	fr::Create(viewport, window_width, window_height);

	fr::Vertex vertices[] = {
		{ { 0.0f, 0.5f, 0.5f } },
		{ { 0.5f, -0.5f, 0.5f } },
		{ { -0.5f, -0.5f, 0.5f } },
	};

	fr::Create(vb, device, vertices, sizeof(vertices), fr::BufferUsageFlag::COPY_DEST | fr::BufferUsageFlag::VERTEX_BUFFER, (int)fr::MemoryProperties::DEVICE_LOCAL);
	fr::StageBuffer(vb, list);

	fr::End(list, render_window);

	fr::ExecuteInfo info{ fence };
	fr::Execute(queue, { list }, info);
	fr::WaitFor(fence[0], device);



	while (!fr::ShouldClose(window)) {
		fr::PollEvents();

		util::update_time();
		util::print_framerate();

		fr::SetClearColor(render_window, { util::sin_pulse(util::time) , util::cos_pulse(util::time), 0, 1 });

		fr::Begin(list, render_window);

		fr::Bind(list, ps);

		fr::Bind(list, viewport);

		fr::BindVertexBuffer(list, vb);

		fr::Draw(list, 3, 1);

		fr::End(list, render_window);

		fr::ExecuteInfo info { fence };
		fr::Execute(queue, { list }, info);
		fr::WaitFor(fence[0], device);

		fr::Present(render_window, queue, device, fence[0]);

		fr::WaitFor(fence[0], device);

		fr::SwapBuffers(window);
	}

	return 0;
}