#include "math/vec.hpp"
#include "math/mat.hpp"
#include <iostream>

int main() {
	fm::vec v (15, 15, 15);
	v.data[0] = 10;
	v += fm::vec(0, 0, 5);
	std::cout << v.x << std::endl;
	std::cout << v.y << std::endl;
	std::cout << v.z << std::endl;

	std::cin.get();

	return 0;
}
