#pragma once

#include "math/vec.hpp"
#include "math/mat.hpp"

class Camera {
protected:
	float _frustum_near;
	float _frustum_far;
	float _fov;
	float _ratio;

	fm::vec _pos;
	fm::vec _euler;

	fm::vec _forward;
	fm::vec _right;
	fm::vec _up;
	fm::vec _world_up;

	fm::mat4 _view;
	fm::mat4 _projection;
	fm::mat4 _pv;

public:
    Camera(float aspect_ratio = 16.0f / 9.0f);
    virtual ~Camera();

	void SetPerspective();
	void SetOrthographic();
    void Update();

	void SetFoV(float fov);
	void SetAspectRatio(float ratio);
	void SetPos(fm::vec pos);
	void SetEuler(fm::vec euler);
	float GetFoV();
	float GetAspectRatio();
	fm::vec GetPos();
	fm::vec GetEuler();
	fm::vec GetForward();
	fm::mat4 GetViewMat();
	fm::mat4 GetProjMat();
	const float* GetViewAsFloat();
	const float* GetProjAsFloat();
	const float* GetPVAsFloat();
	
};
