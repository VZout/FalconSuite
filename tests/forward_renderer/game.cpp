#include "game.hpp"

static uint32_t current_frame_idx = 0;

ForwardRendererExample::ForwardRendererExample() {
	title = "Forward Renderer Example";
}

ForwardRendererExample::~ForwardRendererExample() {
	fr::Destroy(render_target, device);
	fr::Destroy(fence, device);
	fr::Destroy(depth_resource, device);
	fr::Destroy(root_signature, device);

	for (auto list : cmd_list) {
		fr::Destroy(list, device);
	}
}

void ForwardRendererExample::PreInitScene() {
	fr::Allocate(upload_cmd_list, device, 1);
	fr::Begin(upload_cmd_list, render_target, true);

	fr::Create(viewport, window_width, window_height);
}

void ForwardRendererExample::InitSceneImageStuff() {
	for (auto i = 0; i < 3; i++) {
		Allocate(cmd_list[i], device);
	}

	Create(upload_cmd_queue, device, fr::CmdQueueType::COPY);
	Create(present_cmd_queue, device, fr::CmdQueueType::DIRECT);

	fr::Allocate(upload_cmd_list, device, 1);

	fr::ResourceCreateInfo depth_create_info = {};
	depth_create_info.width = window_width;
	depth_create_info.height = window_height;
	depth_create_info.format = fr::FindDepthFormat(device);
	depth_create_info.tiling = fr::ImageTiling::OPTIMAL;
	depth_create_info.usage = fr::ImageUsage::DEPTH;
	depth_create_info.mem_properties = fr::MemoryProperties::DEVICE_LOCAL;
	depth_create_info.aspect = fr::ImageAspect::DEPTH;
	Create(depth_resource, device, depth_create_info);

	fr::RenderTargetAttachement color = { fr::ToFR(render_window.m_swap_chain_image_format), nullptr };
	fr::RenderTargetAttachement depth = { fr::FindDepthFormat(device), &depth_resource };
	std::vector<fr::RenderTargetAttachement> rt_attachements = {
		color,
		depth
	};

	render_target.m_width = window_width;
	render_target.m_height = window_height;
	fr::Create(render_target, device, rt_attachements, render_window.m_swap_chain_images, render_window.m_swap_chain_image_format);
}

void ForwardRendererExample::PrepareAssets() {
	// Load Materials
	for (auto& mat : materials) {
		mat.ta = new fr::TextureArray();
		fr::Load(*mat.ta, mat.texture_paths);

		fr::CreateImages(&upload_cmd_list, *mat.ta, device);

		// TRANSITION TEXTURE ARRAY FOR PERFORMANCE
		fr::Transition(upload_cmd_list, *mat.ta, fr::ResourceState::COPY_DEST, fr::ResourceState::PIXEL_SHADER_RESOURCE);

		mat.vs = std::make_shared<fr::Shader>();
		mat.fs = std::make_shared<fr::Shader>();
		fr::Load(*mat.vs.get(), device, mat.vertex_path);
		fr::Load(*mat.fs.get(), device, mat.fragment_path);

		mat.pso = new fr::PipelineState();
		fr::SetVertexShader(*mat.pso, mat.vs);
		fr::SetFragmentShader(*mat.pso, mat.fs);
		fr::SetRootSignature(*mat.pso, &root_signature);
		fr::Finalize(*mat.pso, device, render_target.m_render_pass);
	}

	for (auto& mat : materials) {
		fr::CreateDescriptorSet(*mat.ta, texture_sampler, device, root_signature);
	}

	// Load Scene Objects
	for (auto& obj : scene_objects) {
		Load(obj.model, obj.model_path);

		obj.vb = new fr::StagingBuffer();
		fr::Create(*obj.vb, device, (void*)obj.model.vertices.data(), sizeof(obj.model.vertices[0]) * obj.model.vertices.size(), fr::BufferUsageFlag::COPY_DEST | fr::BufferUsageFlag::VERTEX_BUFFER, (int)fr::MemoryProperties::DEVICE_LOCAL);

		obj.ib = new fr::StagingBuffer();
		fr::Create(*obj.ib, device, (void*)obj.model.indices.data(), sizeof(obj.model.indices[0]) * obj.model.indices.size(), fr::BufferUsageFlag::COPY_DEST | fr::BufferUsageFlag::INDEX_BUFFER, (int)fr::MemoryProperties::DEVICE_LOCAL);

		fr::StageBuffer(*obj.vb, upload_cmd_list);
		fr::StageBuffer(*obj.ib, upload_cmd_list);
	}
}

void ForwardRendererExample::InitScene() {
	fr::TransitionImageLayout(upload_cmd_list, depth_resource, fr::FindDepthFormat(device), fr::ResourceState::UNKNOWN, fr::ResourceState::DEPTH_WRITE);

	fr::Create(texture_sampler, device);

	// Free Staging Buffers
	//_vb->FreeStagingBuffer();
	//_ib->FreeStagingBuffer();

	fr::Create(root_signature, { 1 });
	fr::Finalize(root_signature, device);

	fr::Create(ub, root_signature, device, nullptr, sizeof(ConstantBufferObject) * 1, fr::BufferUsageFlag::COPY_DEST | fr::BufferUsageFlag::UNIFORM_BUFFER, (int)fr::MemoryProperties::DEVICE_LOCAL);

	// IMGUI
#ifdef FR_USE_IMGUI
	ImGui_ImplGlfwVulkan_Init_Data init_data = {};
	init_data.allocator = NULL;
	init_data.gpu = (VkPhysicalDevice)device.m_physical_device;
	init_data.device = (VkDevice)device.m_native;
	init_data.render_pass = (VkRenderPass)render_target.m_render_pass;
	init_data.pipeline_cache = (VkPipelineCache)pipline_cache;
	init_data.descriptor_pool = (VkDescriptorPool)root_signature.m_desc_pool;
	init_data.check_vk_result = [](VkResult r) { if (r != VK_SUCCESS) std::cout << "A error occured when tryng to use ImGui" << std::endl; };
	ImGui_ImplGlfwVulkan_Init(window, true, &init_data);

	// Upload Fonts
	ImGui_ImplGlfwVulkan_CreateFontsTexture(upload_cmd_list);
#endif

	fr::Create(fence, device);
}

void ForwardRendererExample::PostInitScene() {
	// END UPLOAD COMMAND LIST
	fr::End(upload_cmd_list, true);

	// EXECUTE UPLOAD COMMAND LIST
	fr::Execute(upload_cmd_queue, { upload_cmd_list });
	fr::WaitIdle(upload_cmd_queue);

	cam = new Camera(1280.f / 720.f);
	cam->SetPos(fm::vec(camera_pos[0], camera_pos[1], camera_pos[2]));
	cam->Update();

#ifdef FR_USE_IMGUI
	//img = ImGui_ImplGlfwVulkan_AddTexture(render_system->m_texture_sampler, materials.ta->_texture_image_view[0]);
	ImGui_ImplGlfwVulkan_InvalidateFontUploadObjects();
#endif
}

void ForwardRendererExample::Update() {
	cam->SetPos(fm::vec(camera_pos[0], camera_pos[1], camera_pos[2]));
	cam->Update();

	fm::mat4 view = cam->GetViewMat();
	fm::mat4 proj = cam->GetProjMat();

	ConstantBufferObject cbo;

	static auto startTime = std::chrono::high_resolution_clock::now();
	auto currentTime = std::chrono::high_resolution_clock::now();
	float time = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count() / 1000.0f;

	fm::mat4 model;
	model = fm::mat4::Identity();
	fm::mat4 ro = vik::rotate(model, time * rotate_speed, fm::vec({ 0.0f, -1.0f, 0.0f }));
	fm::mat4 tr = vik::translate(model, fm::vec(0.0f, 0.0f, 0.0f));
	model = ro * tr;

	memcpy(cbo.model, model.data, sizeof(fm::mat4));
	memcpy(cbo.view, view.data, sizeof(fm::mat4));
	memcpy(cbo.proj, proj.data, sizeof(fm::mat4));

	if (auto_light_movement) {
		cbo.lightPos = fm::vec(sin(time) * 4, 2, cos(time) * 3);
	}
	else {
		cbo.lightPos = fm::vec(custom_light_pos[0], custom_light_pos[1], custom_light_pos[2]);
	}

	cbo.time = glfwGetTime() / 70;

	fr::Update(ub, device, sizeof(cbo), &cbo);

	fr::Begin(upload_cmd_list, render_target, true);

	fr::CopyBuffer(upload_cmd_list, *ub.m_staging, *ub.m_buffer, sizeof(cbo));

	fr::End(upload_cmd_list, true);

	// EXECUTE UPLOAD COMMAND LIST
	fr::Execute(upload_cmd_queue, { upload_cmd_list });
	fr::WaitIdle(upload_cmd_queue);
}

void ForwardRendererExample::PreRender() {
	fr::CommandList& current_cmd_list = cmd_list[current_frame_idx];

	fr::Begin(current_cmd_list, render_target);

#ifdef FR_USE_IMGUI
	FillImgUI();
#endif
}

void ForwardRendererExample::Render() {
	fr::CommandList& current_cmd_list = cmd_list[current_frame_idx];

	SceneObj obj = scene_objects[current_scene_obj];
	Material mat = materials[current_material];

	fr::Bind(current_cmd_list, *mat.pso);
	fr::Bind(current_cmd_list, viewport);
	fr::BindVertexBuffer(current_cmd_list, *obj.vb);

	std::array<vk::DescriptorSet, 2> descriptor_sets = { ub.m_desc_set, mat.ta->m_desc_set };
	current_cmd_list.m_native.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, mat.pso->m_layout, 0, descriptor_sets.size(), descriptor_sets.data(), 0, nullptr);

	fr::BindVertexBuffer(current_cmd_list, *obj.vb);
	fr::BindIndexBuffer(current_cmd_list, *obj.ib, 0);
	fr::DrawIndexed(current_cmd_list, obj.model.indices.size(), 1);

#ifdef FR_USE_IMGUI
	ImGui_ImplGlfwVulkan_Render((VkCommandBuffer)current_cmd_list.m_native);
#endif
}

void ForwardRendererExample::PostRender() {
	fr::CommandList& current_cmd_list = cmd_list[current_frame_idx];

	fr::End(current_cmd_list);

	std::vector<fr::CommandList> cmd_lists = { current_cmd_list };
	fr::Execute(present_cmd_queue, cmd_lists);

	fr::Present(present_cmd_queue, device, render_window, fence);
	render_target.m_frame_index = (render_target.m_frame_index + 1) % 3; // Increment frame index

	fr::WaitFor(fence, device);
}

void ForwardRendererExample::OnResize(int width, int height) {
	ExampleBase::OnResize(width, height);

	std::cout << "Window resized to (" << width << "," << height << ")\n"; \

		CleanUpSwapchain();

	fr::Destroy(render_target, device);
	//fr::Destroy(fence, device);
	fr::Destroy(depth_resource, device);
	//fr::Destroy(root_signature, device);

	for (auto list : cmd_list) {
		fr::Destroy(list, device);
	}

	fr::Create(viewport, width, height);

	InitSwapchain();
	InitSceneImageStuff();
}

int main() {
	SetSceneData();
	ForwardRendererExample example;
	example.InitApp();
}