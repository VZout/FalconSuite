#pragma once

#include <string>
#include <vector>
#include <map>

#include "math/vec.hpp"
#include "tiny_obj_loader.h"

namespace fr {

struct Vertex;

struct Model {
	std::vector<Vertex> vertices;
	std::vector<uint16_t> indices;
	std::vector<fm::vec3> normals;
};

float FindCorrispondingTC(tinyobj::attrib_t attrib, tinyobj::mesh_t mesh, float vertex);
bool IsUnique(std::vector<Vertex> vertices, Vertex vertex);
void Load(Model& complete_model, std::map<int, Model>& models, std::string const & path);
void Load(Model& model, std::string const & path);

} /* fr */
