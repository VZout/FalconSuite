#include "camera.hpp"

#define MATH_UTIL_USE_FMATH
#include "math/math_util.hpp"

Camera::Camera(float aspect_ratio) {
	_frustum_near = 0.001f;
	_frustum_far = 1000.f;
	_fov = 70.f;
	_ratio = aspect_ratio;

	SetPerspective();

	_pos = fm::vec(0, 0, -4);
	_world_up = fm::vec(0, 1, 0);
	_euler = fm::vec(0, 0, 0);
	_forward = fm::vec(0, 0, -1);

	_right = _forward.Cross(_world_up).Normalized();
	_up = _right.Cross(_forward).Normalized();
}

Camera::~Camera() {
}

void Camera::SetPerspective() {
	_projection = vik::perspective(_fov, _ratio, _frustum_near, _frustum_far);
}

void Camera::SetOrthographic() {
	
}

void Camera::Update() {
	fm::vec new_forward(0, 0, 0);

	new_forward[2] = cos(vik::rads(_euler[1])) * cos(vik::rads(_euler[0]));
	new_forward[1] = sin(vik::rads(_euler[0]));
	new_forward[0] = sin(vik::rads(_euler[1])) * cos(vik::rads(_euler[0]));

	_forward = new_forward.Normalized();
	_right = _forward.Cross(_world_up).Normalized();
	_up = _right.Cross(_forward).Normalized();

	_view = vik::look_at<>(_pos, _pos + _forward, _up);
	_pv = _projection * _view;
}

void Camera::SetFoV(float fov) {
	_fov = fov;
	_projection = vik::perspective<>(_fov, _ratio, _frustum_near, _frustum_far);
}

void Camera::SetAspectRatio(float ratio) {
	_ratio = ratio;
	_projection = vik::perspective<>(_fov, _ratio, _frustum_near, _frustum_far);
}

void Camera::SetPos(fm::vec pos) {
	_pos = pos;
}

void Camera::SetEuler(fm::vec euler) {
	_euler = euler;
}

float Camera::GetFoV() {
	return _fov;
}

float Camera::GetAspectRatio() {
	return _ratio;
}

fm::vec Camera::GetPos() {
	return _pos;
}

fm::vec Camera::GetEuler() {
	return _euler;
}

fm::vec Camera::GetForward() {
	return _forward;
}

fm::mat4 Camera::GetViewMat() {
	return _view;
}

fm::mat4 Camera::GetProjMat() {
	return _projection;
}

const float* Camera::GetViewAsFloat() {
	//return glm::value_ptr(_view);
	return 0;
}

const float* Camera::GetProjAsFloat() {
	//return glm::value_ptr(_projection);
	return 0;
}

const float* Camera::GetPVAsFloat() {
	//return glm::value_ptr(_pv);
	return 0;
}