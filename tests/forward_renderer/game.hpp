#pragma once

#define FR_USE_VULKAN
#define FR_USE_IMGUI

#include "../common/exampleapp.hpp"

#include <memory>
#include <chrono>

#define MATH_UTIL_USE_FMATH
#include <math/vec.hpp>
#include <math/mat.hpp>
#include <math/math_util.hpp>

#include <renderer/interface.hpp>
#include "camera.hpp"
#include "model.hpp"
#include <renderer/vertex.hpp>

#include <renderer/vk/imgui/imgui.h>

#include <GLFW/glfw3.h>

struct SceneObj {
	// info
	std::string model_path;

	// data
	fr::Model model;
	fr::StagingBuffer* vb;
	fr::StagingBuffer* ib;
};

struct Material {
	std::string name = "Unknown";
	std::vector<std::string> texture_paths;
	std::string vertex_path;
	std::string fragment_path;

	fr::PipelineState* pso;
	fr::TextureArray* ta;
	std::shared_ptr<fr::Shader> vs;
	std::shared_ptr<fr::Shader> fs;
};

struct ConstantBufferObject {
	float model[16];
	float view[16];
	float proj[16];
	fm::vec3 lightPos;
	float time;
};

static int current_scene_obj = 0;
static int current_material = 0;
static float rotate_speed = 0.0125;
static bool auto_light_movement = true;
static float custom_light_pos[3] = { 2, 0, -2 };
static float camera_pos[3] = { 0, 0, -1.5f };
static std::vector<SceneObj> scene_objects;
static std::vector<Material> materials;
static Camera* cam;

//ImGUI
static void* img;
static VkPipelineCache pipline_cache = VK_NULL_HANDLE;

bool IMGUI_ShowAbout = false;
bool IMGUI_ShowPerformance = true;
bool IMGUI_SceneSettings = false;
bool IMGUI_ShowTechnicalSupport = false;

void FillImgUI() {
	ImGui_ImplGlfwVulkan_NewFrame();

	if (ImGui::BeginMainMenuBar()) {
		if (ImGui::BeginMenu("File")) {
			ImGui::MenuItem("Screenshot", "F12", &IMGUI_ShowAbout);
			ImGui::MenuItem("Quit", "Alt+F4", &IMGUI_ShowAbout);
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("View")) {
			ImGui::MenuItem("Viewport Settings", NULL, &IMGUI_ShowAbout);
			ImGui::MenuItem("Scene Settings", NULL, &IMGUI_SceneSettings);
			ImGui::MenuItem("Performance Overlay", NULL, &IMGUI_ShowPerformance);
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Help")) {
			ImGui::MenuItem("Tenhnical Support", NULL, &IMGUI_ShowAbout);
			ImGui::MenuItem("About", NULL, &IMGUI_ShowAbout);
			ImGui::EndMenu();
		}
		ImGui::EndMainMenuBar();
	}

	if (IMGUI_ShowPerformance) {
		ImGui::SetNextWindowPos(ImVec2(0, 20));
		ImGui::Begin("Performance Overlay", &IMGUI_ShowPerformance, ImVec2(0, 0), 0.3f, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings);
		ImGui::Text("Display Size: (%.0f,%.0f)", ImGui::GetIO().DisplaySize.x, ImGui::GetIO().DisplaySize.y);
		ImGui::Text("Frames per Second: %.0f", ImGui::GetIO().Framerate);
		ImGui::End();
	}

	if (IMGUI_SceneSettings) {
		ImGui::Begin("Scene Settings", &IMGUI_SceneSettings, ImGuiWindowFlags_AlwaysAutoResize);

		{
			std::vector<const char*> so_items;
			for (auto i = 0; i < scene_objects.size(); i++) {
				so_items.push_back(scene_objects[i].model_path.c_str());
			}
			ImGui::Combo("Scene Object", &current_scene_obj, so_items.data(), so_items.size());
		}

		{
			std::vector<const char*> m_items;
			for (auto i = 0; i < materials.size(); i++) {
				m_items.push_back(materials[i].name.c_str());
			}
			ImGui::Combo("Material", &current_material, m_items.data(), m_items.size());
		}

		ImGui::DragFloat("Rotate Speed", &rotate_speed, 0.1, 0, 2);
		ImGui::DragFloat3("Light Pos", custom_light_pos, 0.1);
		ImGui::Checkbox("Use Automatic Light Movement", &auto_light_movement);
		ImGui::DragFloat3("Camera Pos", camera_pos, 0.1);

		ImGui::End();
	}

	if (IMGUI_ShowAbout) {
		ImGui::Begin("About ImGui", &IMGUI_ShowAbout, ImGuiWindowFlags_AlwaysAutoResize);
		ImGui::Text("IMGUI Version: %s", IMGUI_VERSION);
		ImGui::Text("Falcon Renderer: %s", "0.0.1");
		ImGui::Separator();
		ImGui::Text("Copyright Viktor Zoutman 2017.");
		ImGui::Text("FalconRenderer is licensed under the MIT License.");
		ImGui::End();
	}

	if (IMGUI_ShowTechnicalSupport) {
		ImGui::Begin("Technical Support", &IMGUI_ShowTechnicalSupport, ImGuiWindowFlags_AlwaysAutoResize);
		ImGui::Text("viktorzoutman@vzout.com");
		ImGui::Text("https://gitlab.com/VZout/FalconSuite");
		ImGui::End();
	}
}

void SetSceneData() {
	/* = scene objects = */
	SceneObj sphere;
	sphere.model_path = "resources/sphere.obj";
	SceneObj cube;
	cube.model_path = "resources/cube.obj";
	SceneObj torus;
	torus.model_path = "resources/torus.obj";
	SceneObj spot;
	spot.model_path = "resources/spot.obj";
	SceneObj dragon;
	dragon.model_path = "resources/dragon.obj";
	scene_objects.push_back(sphere);
	scene_objects.push_back(cube);
	scene_objects.push_back(torus);
	scene_objects.push_back(spot);
	scene_objects.push_back(dragon);
	/* ================= */

	/* = Materials = */
	Material planet_mat;
	planet_mat.name = "Planet";
	planet_mat.texture_paths = { "resources/texture.jpg", "resources/clouds.jpg", "resources/night.jpg" };
	planet_mat.fragment_path = "resources/frag.spv";
	planet_mat.vertex_path = "resources/vert.spv";
	Material toon_mat;
	toon_mat.name = "Toon";
	toon_mat.texture_paths = { "resources/spot_texture.png", "resources/toon_gradiant.jpg", "resources/night.jpg" };
	toon_mat.fragment_path = "resources/toon_frag.spv";
	toon_mat.vertex_path = "resources/toon_vert.spv";
	materials.push_back(planet_mat);
	materials.push_back(toon_mat);
	/* ================= */
}

class ForwardRendererExample : public ExampleBase {
protected:
	fr::CommandQueue upload_cmd_queue;
	fr::CommandQueue present_cmd_queue;

	fr::RootSignature root_signature;
	fr::Viewport viewport;
	fr::CommandList cmd_list[3];
	fr::CommandList upload_cmd_list;
	fr::Resource depth_resource;
	fr::RenderTarget render_target;
	fr::Fence fence;
	fr::TextureSampler texture_sampler;
	
	fr::UniformStagingBuffer ub;

public:
	ForwardRendererExample();
	virtual ~ForwardRendererExample();

	virtual void OnResize(int width, int height);
	virtual void LoadAssets() {}
	virtual void PrepareAssets();
	virtual void InitSceneImageStuff();
	virtual void InitScene();
	virtual void PreInitScene();
	virtual void PostInitScene();
	virtual void Update();
	virtual void PreRender();
	virtual void Render();
	virtual void PostRender();
};