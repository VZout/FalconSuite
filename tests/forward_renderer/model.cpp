#include "model.hpp"

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <sstream>

#include <renderer/vertex.hpp>

namespace fr {

float FindCorrispondingTC(tinyobj::attrib_t attrib, tinyobj::mesh_t mesh, float vertex) {
	for (size_t i = 0; i < mesh.indices.size(); i++) {
		int idx = mesh.indices[i].vertex_index;
		if (attrib.vertices[i] == vertex) {
			return attrib.texcoords[mesh.indices[i].texcoord_index];
		}
	}
}

bool IsUnique(std::vector<Vertex> vertices, Vertex vertex) {
	for (auto v : vertices) {
		if (v == vertex) {
			return false;
		}
	}

	return true;
}

void Load(Model& complete_model, std::map<int, Model>& models, std::string const & path) {
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	//std::stringstream ss_obj(m_buffer);


	std::vector<unsigned int> indices;

	std::string err;
	//bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, ss_obj);
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, path.c_str());
	if (!err.empty() || !ret) {
		std::cerr << "Error loading obj from memory:" << err << std::endl;
	}

	std::unordered_map<Vertex, int> uniqueVertices = {};
	for (auto shape : shapes) {
		size_t index_offset = 0;
		for (size_t f = 0; f < shape.mesh.num_face_vertices.size(); f++) {
			int fv = shape.mesh.num_face_vertices[f];
			int mat_id = shape.mesh.material_ids[f];

			if (models.find(mat_id) == models.end()) {
				models.insert(std::pair<int, Model>(mat_id, Model()));
			}
			for (size_t v = 0; v < fv; v++) {

				Vertex vertex = {};

				tinyobj::index_t index = shape.mesh.indices[index_offset + v];

				vertex.m_pos = {
					attrib.vertices[3 * index.vertex_index + 0],
					attrib.vertices[3 * index.vertex_index + 1],
					attrib.vertices[3 * index.vertex_index + 2]
				};

				vertex.m_normal = {
					attrib.normals[3 * index.normal_index + 0],
					attrib.normals[3 * index.normal_index + 1],
					attrib.normals[3 * index.normal_index + 2]
				};

				vertex.m_texCoord = {
					attrib.texcoords[2 * index.texcoord_index + 0],
					1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
				};

				//if (uniqueVertices.count(vertex) == 0) {
				//	uniqueVertices[vertex] = complete_model.vertices.size();
					complete_model.vertices.push_back(vertex);
				//}

				//complete_model.indices.push_back(uniqueVertices[vertex]);

				unsigned int iii = indices.size();

				indices.push_back(iii);
				models[mat_id].indices.push_back(iii);
			}

			index_offset += fv;
		}
	}
}

void Load(Model& model, std::string const & path) {
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	//std::stringstream ss_obj(m_buffer);

	std::string err;
	//bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, ss_obj);
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, path.c_str());
	if (!err.empty() || !ret) {
		std::cerr << "Error loading obj from memory:" << err << std::endl;
	}

	std::unordered_map<Vertex, int> uniqueVertices = {};
	for (auto shape : shapes) {
		for (auto index : shape.mesh.indices) {
			Vertex vertex = {};

			vertex.m_pos = {
				attrib.vertices[3 * index.vertex_index + 0],
				attrib.vertices[3 * index.vertex_index + 1],
				attrib.vertices[3 * index.vertex_index + 2]
			};

			vertex.m_normal = {
				attrib.normals[3 * index.normal_index + 0],
				attrib.normals[3 * index.normal_index + 1],
				attrib.normals[3 * index.normal_index + 2]
			};

			vertex.m_texCoord = {
				attrib.texcoords[2 * index.texcoord_index + 0],
				1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
			};

			if (uniqueVertices.count(vertex) == 0) {
				uniqueVertices[vertex] = model.vertices.size();
				model.vertices.push_back(vertex);
			}

			model.indices.push_back(uniqueVertices[vertex]);
		}
	}
}

} /* fr */
