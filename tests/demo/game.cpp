#include "game.hpp"

#include <app.hpp>
#include <vk_render_system.hpp>
#include <camera.hpp>
#include <vertex.hpp>
#include <vk_texture_image.hpp>
#include <vk_staging_buffer.hpp>
#include <vk_uniform_staging_buffer.hpp>
#include <vk_pipeline_state.hpp>
#include <vk_root_signature.hpp>
#include <res/res_cache_manager.hpp>
#include <res/res_cache.hpp>
#include <res/shader.hpp>
#include <res/model.hpp>
#include <window.hpp>
#include <chrono>
#define MATH_UTIL_USE_FMATH
#include <util/math_util.hpp>

#include "math/mat.hpp"

DemoGame::DemoGame(VKRenderSystem& render_system) : _render_system(render_system) {
}

DemoGame::~DemoGame() {
	delete _root_signature;
	delete _pso;
	delete _cam;

	delete _texture;
	delete _vb;
	delete _ib;
	delete _ub;
}

void DemoGame::StageFunc(vk::CommandBuffer cmd_buffer) {
	_root_signature = new VKRootSignature(_render_system);
	_root_signature->SetNumConstantBuffers(1);
	_root_signature->Finalize();	

	ResourceCacheManager& rc_manager = ResourceCacheManager::GetInstance();
	std::shared_ptr<Model> obj = rc_manager.GetCache("common")->GetHandle<Model>(new Resource("test.obj"));

	_texture = new VKTextureImage(_render_system);
	_texture->Compile(_root_signature);

	_vb = new VKStagingBuffer(_render_system);
	_vb->Init(sizeof(obj->vertices[0]) * obj->vertices.size(), (void*)obj->vertices.data(), vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eVertexBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal);

	_ib = new VKStagingBuffer(_render_system);
	_ib->Init(sizeof(obj->indices[0]) * obj->indices.size(), (void*)obj->indices.data(), vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eIndexBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal);

	_ub = new VKUniformStagingBuffer(_render_system);
	_ub->Init(_root_signature, sizeof(ConstantBufferObject) * 1, nullptr, vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal);

	_vb->StageBuffer(cmd_buffer);
	_ib->StageBuffer(cmd_buffer);
}

void DemoGame::FillFunc(vk::CommandBuffer cmd_buffer) {
	if (!_pso) {
		ResourceCacheManager& rc_manager = ResourceCacheManager::GetInstance();
		std::shared_ptr<Shader> vs = rc_manager.GetCache("common")->GetHandle<Shader>(new Resource("vert.spv"));
		std::shared_ptr<Shader> fs = rc_manager.GetCache("common")->GetHandle<Shader>(new Resource("frag.spv"));

		_pso = new VKPipelineState(_render_system);
		_pso->SetVertexShader(vs);
		_pso->SetFragmentShader(fs);
		_pso->SetRootSignature(_root_signature);
		_pso->Finalize(_render_system._render_pass);
	}

	// do some actual commands.
	cmd_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, _pso->GetNative());

	vk::Buffer vertex_buffers[] = { _vb->GetNative() };
	vk::DeviceSize offsets[] = { 0 };

	cmd_buffer.bindVertexBuffers(0, 1, vertex_buffers, offsets);
	cmd_buffer.bindIndexBuffer(_ib->GetNative(), 0, vk::IndexType::eUint16);

	{
		std::array<vk::DescriptorSet, 2> descriptor_sets = { _ub->GetDescriptorSet(), _texture->GetDescriptorSet() };
		cmd_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, _pso->GetLayout(), 0, descriptor_sets.size(), descriptor_sets.data(), 0, nullptr);
	}

	ResourceCacheManager& rc_manager = ResourceCacheManager::GetInstance();
	std::shared_ptr<Model> obj = rc_manager.GetCache("common")->GetHandle<Model>(new Resource("test.obj"));
	cmd_buffer.drawIndexed(obj->indices.size(), 1, 0, 0, 0);
}

void DemoGame::Init() {
	_cam = new Camera(1280.f / 720.f);
	_cam->SetPos(fm::vec(0, 0, -1.5f));

	_vb->FreeStagingBuffer();
	_ib->FreeStagingBuffer();
}
	
void DemoGame::Update(float delta) {
	_cam->Update();

	fm::mat4 view = _cam->GetViewMat();
	fm::mat4 proj = _cam->GetProjMat();

	ConstantBufferObject cbo;

	static auto startTime = std::chrono::high_resolution_clock::now();
	auto currentTime = std::chrono::high_resolution_clock::now();
	float time = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime).count() / 1000.0f;

	fm::mat4 model;
	model = fm::mat4::Identity();
	fm::mat4 ro = vik::rotate(model, time / 80, fm::vec({ 0.0f, -1.0f, 0.0f }));
	fm::mat4 tr = vik::translate(model, fm::vec(0.0f, 0.0f, 0.0f));
	model = ro * tr;

	memcpy(cbo.model, model.data, sizeof(fm::mat4));
	memcpy(cbo.view, view.data, sizeof(fm::mat4));
	memcpy(cbo.proj, proj.data, sizeof(fm::mat4));
	cbo.lightPos = fm::vec(sin(time)*4, 2, cos(time) * 3);
	cbo.time = glfwGetTime() / 70;

	_ub->Update(sizeof(cbo), &cbo);
	_render_system.CopyBuffer(_ub->GetStagingNative(), _ub->GetNative(), sizeof(cbo));
}	

void DemoGame::Render() {

}

int main() {
	Application* app = new Application();

	using std::placeholders::_1;

	app->Create("Planet Earth (Vk)");

	DemoGame* game = new DemoGame(*app->_render_system);
	app->_render_system->_fill_func = std::bind(&DemoGame::FillFunc, game, _1);
	app->_render_system->_stage_buffers_func = std::bind(&DemoGame::StageFunc, game, _1);
	app->_render_system->UploadBuffers();
	app->_render_system->CreateCommandBuffers();

	app->Start(std::bind(&DemoGame::Init, game), std::bind(&DemoGame::Update, game, _1), std::bind(&DemoGame::Render, game));

	delete game;

	app->_window->Close();
	app->_window.reset();
	app->_render_system.reset();

	delete app;

	return 0;
}
