#pragma once

class VKStagingBuffer;
class VKUniformStagingBuffer;
class VKRenderSystem;
class VKRootSignature;
class VKPipelineState;
class VKTextureImage;
class Camera;

#include <vulkan\vulkan.hpp>

class DemoGame {
private:
	VKRenderSystem& _render_system;

	Camera* _cam;

	VKStagingBuffer* _vb;
	VKStagingBuffer* _ib;
	VKUniformStagingBuffer* _ub;
	VKTextureImage* _texture;

	VKRootSignature* _root_signature;
	VKPipelineState* _pso = nullptr;

public:
	DemoGame(VKRenderSystem& render_system);
	virtual ~DemoGame();

	void StageFunc(vk::CommandBuffer cmd_buffer);
	void FillFunc(vk::CommandBuffer cmd_buffer);

	void Init();
	void Update(float delta);
	void Render();
};