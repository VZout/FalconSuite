#include "game.hpp"

static uint32_t current_frame_idx = 0;

ResizeExample::ResizeExample() {
	title = "Resize Example";
}

ResizeExample::~ResizeExample() {
	fr::Destroy(render_target, device);
	fr::Destroy(fence, device);
	fr::Destroy(depth_resource, device);
	fr::Destroy(root_signature, device);

	for (auto list : cmd_list) {
		fr::Destroy(list, device);
	}
}

void ResizeExample::PreInitScene() {
	fr::Allocate(upload_cmd_list, device, 1);
	fr::Begin(upload_cmd_list, render_target, true);

	fr::Create(viewport, window_width, window_height);
}

void ResizeExample::InitSceneImageStuff() {
	for (auto i = 0; i < 3; i++) {
		Allocate(cmd_list[i], device);
	}

	Create(upload_cmd_queue, device, fr::CmdQueueType::COPY);
	Create(present_cmd_queue, device, fr::CmdQueueType::DIRECT);

	fr::Allocate(upload_cmd_list, device, 1);

	fr::ResourceCreateInfo depth_create_info = {};
	depth_create_info.width = window_width;
	depth_create_info.height = window_height;
	depth_create_info.format = fr::FindDepthFormat(device);
	depth_create_info.tiling = fr::ImageTiling::OPTIMAL;
	depth_create_info.usage = fr::ImageUsage::DEPTH;
	depth_create_info.mem_properties = fr::MemoryProperties::DEVICE_LOCAL;
	depth_create_info.aspect = fr::ImageAspect::DEPTH;
	Create(depth_resource, device, depth_create_info);

	fr::RenderTargetAttachement color = { fr::ToFR(render_window.m_swap_chain_image_format), nullptr };
	fr::RenderTargetAttachement depth = { fr::FindDepthFormat(device), &depth_resource };
	std::vector<fr::RenderTargetAttachement> rt_attachements = {
		color,
		depth
	};

	render_target.m_width = window_width;
	render_target.m_height = window_height;
	fr::Create(render_target, device, rt_attachements, render_window.m_swap_chain_images, render_window.m_swap_chain_image_format);
}

void ResizeExample::InitScene() {
	fr::TransitionImageLayout(upload_cmd_list, depth_resource, fr::FindDepthFormat(device), fr::ResourceState::UNKNOWN, fr::ResourceState::DEPTH_WRITE);

	fr::Create(root_signature, { 1 });
	fr::Finalize(root_signature, device);

	fr::Create(fence, device);
}

void ResizeExample::PostInitScene() {
	// END UPLOAD COMMAND LIST
	fr::End(upload_cmd_list, true);

	// EXECUTE UPLOAD COMMAND LIST
	fr::Execute(upload_cmd_queue, { upload_cmd_list });
	fr::WaitIdle(upload_cmd_queue);
}

void ResizeExample::Update() {
}

void ResizeExample::PreRender() {
	fr::CommandList& current_cmd_list = cmd_list[current_frame_idx];

	fr::Begin(current_cmd_list, render_target);
}

void ResizeExample::Render() {
}

void ResizeExample::PostRender() {
	fr::CommandList& current_cmd_list = cmd_list[current_frame_idx];

	fr::End(current_cmd_list);

	std::vector<fr::CommandList> cmd_lists = { current_cmd_list };
	fr::Execute(present_cmd_queue, cmd_lists);

	fr::Present(present_cmd_queue, device, render_window, fence);
	render_target.m_frame_index = (render_target.m_frame_index + 1) % 3; // Increment frame index

	fr::WaitFor(fence, device);
	fr::WaitIdle(present_cmd_queue);
}

void ResizeExample::OnResize(int width, int height) {
	ExampleBase::OnResize(width, height);

	std::cout << "Window resized to (" << width << "," << height << ")\n"; \

	CleanUpSwapchain();

	fr::Destroy(render_target, device);
	//fr::Destroy(fence, device);
	fr::Destroy(depth_resource, device);
	//fr::Destroy(root_signature, device);

	for (auto list : cmd_list) {
		fr::Destroy(list, device);
	}

	InitSwapchain();
	InitSceneImageStuff();
}

int main() {
	ResizeExample example;
	example.InitApp();
}
