#pragma once

#define FR_USE_VULKAN

#include "../common/exampleapp.hpp"

class ResizeExample : public ExampleBase {
protected:
	fr::CommandQueue upload_cmd_queue;
	fr::CommandQueue present_cmd_queue;

	fr::Window window;
	fr::RootSignature root_signature;
	fr::Viewport viewport;
	fr::CommandList cmd_list[3];
	fr::CommandList upload_cmd_list;
	fr::Resource depth_resource;
	fr::RenderTarget render_target;
	fr::Fence fence;

public:
	ResizeExample();
	virtual ~ResizeExample();

	virtual void OnResize(int width, int height);
	virtual void LoadAssets() {}
	virtual void PrepareAssets() {}
	virtual void InitSceneImageStuff();
	virtual void InitScene();
	virtual void PreInitScene();
	virtual void PostInitScene();
	virtual void Update();
	virtual void PreRender();
	virtual void Render();
	virtual void PostRender();
};