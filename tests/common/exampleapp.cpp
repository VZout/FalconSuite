#include "exampleapp.hpp"

ExampleBase::ExampleBase() {

}

ExampleBase::~ExampleBase() {
	CleanUpSwapchain();

	fr::Destroy(device, instance);
	fr::Destroy(instance);
	fr::Destroy(window);
}

void ExampleBase::InitApp() {
	fr::WindowCreateInfo create_info;
	create_info.title = title.c_str();
	create_info.fullscreen = false;
	fr::Create(window, create_info);

	fr::BindOnResize(window, [&](int width, int height) {
		OnResize(width, height);
	});

	fr::Create(instance);
	fr::PreInitialize(render_window, instance, window);
	fr::Create(device, instance, render_window);
	fr::Create(render_window, device, window_width, window_height);

	InitSceneImageStuff();
	PreInitScene();
	LoadAssets();
	InitScene();
	PrepareAssets();
	PostInitScene();

	while (!fr::ShouldClose(window)) {
		GameLoop();
	}
}

void ExampleBase::OnResize(int width, int height) {
	window_width = width;
	window_height = height;
}

void ExampleBase::InitSwapchain() {
	fr::PreInitialize(render_window, instance, window);
	fr::Query(device, instance, render_window);
	fr::Create(render_window, device, window_width, window_height);
}

void ExampleBase::CleanUpSwapchain() {
	fr::Destroy(render_window, device, instance);
}

void ExampleBase::GameLoop() {
	fr::PollEvents();

#ifdef FR_USE_IMGUI
	FillImgUI();
#endif

	Update();
	PreRender();
	Render();
	PostRender();

	fr::SwapBuffers(window);
}
