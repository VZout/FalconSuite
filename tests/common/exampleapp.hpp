#pragma once

#define FR_USE_VULKAN

#include <renderer/interface.hpp>
#include <renderer/vertex.hpp>
#include <renderer/vk/imgui/imgui.h>
#include <renderer/vk/imgui/imgui_impl_glfw_vulkan.h>

class ExampleBase {
protected:
	fr::Window window;
	fr::Instance instance;
	fr::RenderWindow render_window;
	fr::Device device;

	int window_width = 1280, window_height = 720;
	std::string title = "Unnamed Example Application";

public:
	ExampleBase();
	virtual ~ExampleBase();

	// App Functions
	virtual void InitApp();
	virtual void InitSwapchain();
	virtual void CleanUpSwapchain();
	virtual void GameLoop();
	virtual void OnResize(int width, int height);

	// Scene Fucntions
	virtual void LoadAssets() = 0;
	virtual void PrepareAssets() = 0;
	virtual void InitSceneImageStuff() = 0;
	virtual void InitScene() = 0;
	virtual void PreInitScene() = 0;
	virtual void PostInitScene() = 0;
	virtual void Update() = 0;
	virtual void PreRender() = 0;
	virtual void Render() = 0;
	virtual void PostRender() = 0;
};
