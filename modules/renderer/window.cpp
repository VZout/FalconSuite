#include "window.hpp"

#include <stdexcept>
#include <unordered_map>

#define GLFW_EXPOSE_NATIVE_WIN32

#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

namespace fr {

void Create(Window& window, WindowCreateInfo const& create_info) {
	if (!glfwInit()) {
		throw std::runtime_error("GLFW initialization failed");
	}

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

	if (!create_info.fullscreen) {
		window.m_native = glfwCreateWindow(create_info.width,
			create_info.height,
			create_info.title.c_str(),
			NULL,
			NULL);
	}
	else {
		window.m_native = glfwCreateWindow(create_info.width,
			create_info.height,
			create_info.title.c_str(),
			glfwGetPrimaryMonitor(),
			NULL);
	}

    if (!window.m_native) {
        glfwTerminate();
        throw std::runtime_error("GLFW window creation failed");
    }
}

void Destroy(Window& window) {
	glfwDestroyWindow(window.m_native);
	glfwTerminate();
}

std::unordered_map<GLFWwindow*, std::function<void(int, int)>> callbacks;
void BindOnResize(Window& window, std::function<void(int width, int height)> callback) {
	callbacks[window.m_native] = std::move(callback);

	glfwSetWindowSizeCallback(window.m_native, [](GLFWwindow* window, int width, int height) {
		callbacks[window](width, height);
	});
}

void PollEvents() {
	glfwPollEvents();
}

void SwapBuffers(Window& window) {
	glfwSwapBuffers(window.m_native);
}

void Close(Window& window) {
	glfwSetWindowShouldClose(window.m_native, GLFW_TRUE);
}

bool ShouldClose(Window& window) {
	return glfwWindowShouldClose(window.m_native);
}

const char** GetRequiredInstanceExtensions(unsigned int* count) {
	return glfwGetRequiredInstanceExtensions(count);
}

} /* fr */
