#pragma once

#include <vulkan/vulkan.hpp>

#include "../interface.hpp"

namespace fr {

struct _Device {
	vk::Device m_native;
	vk::PhysicalDevice m_physical_device;
	vk::CommandPool m_cmd_pool;

	QueueFamilyIndices m_queue_family_indices;
	SwapChainSupportDetails m_swap_chain_support_details;
	std::vector<const char*> m_device_extensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME, VK_KHR_MAINTENANCE1_EXTENSION_NAME}; // VK_KHR_MAINTENANCE1_EXTENSION_NAME Allows for negative viewport height.
	VkDebugReportCallbackEXT m_debug_callback = VK_NULL_HANDLE;
	const std::vector<const char*> m_validation_layers = { "VK_LAYER_LUNARG_standard_validation" };
};

} /* fr */