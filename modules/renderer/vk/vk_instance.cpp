#include "vk_instance.hpp"

#include "vk_internal.hpp"

namespace fr {

	void Create(Instance& inst) {
		vk::ApplicationInfo app_info = {};
		app_info.pApplicationName = "Vulkan application";
		app_info.applicationVersion = VK_MAKE_VERSION(0, 0, 1);
		app_info.pEngineName = "Falcon";
		app_info.engineVersion = VK_MAKE_VERSION(0, 0, 1);
		app_info.apiVersion = VK_API_VERSION_1_0;

		std::vector<const char*> extensions = internal_vk::GetRequiredExtensions();

		vk::InstanceCreateInfo create_info = {};
		create_info.pApplicationInfo = &app_info;
		create_info.enabledExtensionCount = extensions.size();
		create_info.ppEnabledExtensionNames = extensions.data();

#ifdef ENABLE_VK_VALIDATION_LAYERS
		create_info.enabledLayerCount = inst.validation_layers.size();
		create_info.ppEnabledLayerNames = inst.validation_layers.data();
#else
		create_info.enabledLayerCount = 0;
#endif

		vk::Result r = vk::createInstance(&create_info, nullptr, &inst.m_native);
		if (r != vk::Result::eSuccess) {
			throw std::runtime_error("Failed to create vulkan instance");
		}
	}

	void Destroy(Instance& inst) {
		inst.m_native.destroy();
	}

} /* fr */
