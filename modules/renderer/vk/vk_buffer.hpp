#pragma once

#include "../interface.hpp"

#include <vulkan/vulkan.hpp>

namespace fr {

	struct _Buffer {
		vk::Buffer m_native;
		vk::DeviceMemory m_memory;
	};

} /* fr */
