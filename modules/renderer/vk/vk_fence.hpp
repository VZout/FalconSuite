#pragma once

#include <vulkan/vulkan.hpp>

#include "../interface.hpp"

namespace fr {

struct _Fence {
	vk::Fence m_native;
};

} /* fr */
