#include "vk_staging_buffer.hpp"

#include "vk_device.hpp"

namespace fr {

void Create(StagingBuffer& buffer, Device& device, void* data, uint64_t size, unsigned char usage_flags, int properties) {
	buffer.m_size = size;

	vk::Device n_device = device.m_native;

	buffer.m_buffer = new Buffer();
	buffer.m_staging = new Buffer();

	Create(*buffer.m_staging, device, size, BufferUsageFlag::COPY_SOURCE, (int)vk::MemoryPropertyFlagBits::eHostVisible | (int)vk::MemoryPropertyFlagBits::eHostCoherent);

	if (data != nullptr) {
		void* temp_data;
		n_device.mapMemory(buffer.m_staging->m_memory, 0, size, vk::MemoryMapFlagBits(0), &temp_data); //FIXME	wtf vulkan... memorymapflagbits is not implemented hence why I can't just type 0. Now I have to type vk::MemoryMapFlagBits(0)
		memcpy(temp_data, data, (size_t)size);
		n_device.unmapMemory(buffer.m_staging->m_memory);
	}

	Create(*buffer.m_buffer, device, size, usage_flags, properties);
}

void Destroy(StagingBuffer& buffer, Device& device) {
	vk::Device n_device = device.m_native;

	Destroy(*buffer.m_buffer, device);
}


void Create(UniformStagingBuffer& buffer, RootSignature& root_signature, Device& device, void* data, uint64_t size, unsigned char usage_flags, int properties) {
	Create(buffer, device, data, size, usage_flags, properties);

	// Create descripor set
	vk::Device n_device = device.m_native;

	vk::DescriptorSetLayout layouts[] = { root_signature.m_desc_set_layout };

	vk::DescriptorSetAllocateInfo alloc_info = {};
	alloc_info.descriptorPool = root_signature.m_desc_pool;
	alloc_info.descriptorSetCount = 1;
	alloc_info.pSetLayouts = layouts;

	vk::Result r = n_device.allocateDescriptorSets(&alloc_info, &buffer.m_desc_set);
	if (r != vk::Result::eSuccess) {
		throw std::runtime_error("failed to create descriptor set.");
	}

	vk::DescriptorBufferInfo buffer_info = {};
	buffer_info.buffer = buffer.m_buffer->m_native;
	buffer_info.offset = 0;
	buffer_info.range = size; //FIXME: hardcoded

	std::array<vk::WriteDescriptorSet, 1> descriptorWrites = {};

	descriptorWrites[0].dstSet = buffer.m_desc_set;
	descriptorWrites[0].dstBinding = 0;
	descriptorWrites[0].dstArrayElement = 0;
	descriptorWrites[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	descriptorWrites[0].descriptorCount = 1;
	descriptorWrites[0].pBufferInfo = &buffer_info;

	n_device.updateDescriptorSets(descriptorWrites.size(), descriptorWrites.data(), 0, nullptr);
}

void Destroy(UniformStagingBuffer& buffer, RootSignature& root_signature, Device& device) {
	vk::Device n_device = device.m_native;

	Destroy(*buffer.m_staging, device);
	Destroy(*buffer.m_buffer, device);

	vk::DescriptorSet sets[1] = { buffer.m_desc_set };
	n_device.freeDescriptorSets(root_signature.m_desc_pool, 1, sets);
}

void Update(StagingBuffer& buffer, Device& device, vk::DeviceSize size, void* data) {
	vk::Device n_device = device.m_native;

	void* temp_data;
	n_device.mapMemory(buffer.m_staging->m_memory, 0, size, vk::MemoryMapFlagBits(0), &temp_data); //FIXME	wtf vulkan... memorymapflagbits is not implemented hence why I can't just type 0. Now I have to type vk::MemoryMapFlagBits(0)
	memcpy(temp_data, data, (size_t)size);
	n_device.unmapMemory(buffer.m_staging->m_memory);
}

void StageBuffer(StagingBuffer& buffer, CommandList& cmd_list) {
	vk::CommandBuffer n_cmd_list = cmd_list.m_native[cmd_list.m_current_frame_idx];

	vk::BufferCopy copyRegion = {};
	copyRegion.srcOffset = 0; // Optional
	copyRegion.dstOffset = 0; // Optional
	copyRegion.size = buffer.m_size;

	n_cmd_list.copyBuffer(buffer.m_staging->m_native, buffer.m_buffer->m_native, 1, &copyRegion);
}

void FreeStagingBuffer(StagingBuffer& buffer, Device& device) {
	vk::Device n_device = device.m_native;

	if (buffer.m_staging->m_native) n_device.destroyBuffer(buffer.m_staging->m_native);
	if (buffer.m_staging->m_memory) n_device.freeMemory(buffer.m_staging->m_memory);
}

} /* fr */
