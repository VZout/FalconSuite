#pragma once

#include "../interface.hpp"

#include <vulkan/vulkan.hpp>

namespace fr {

struct _Viewport {
	vk::Viewport m_viewport;
	vk::Rect2D m_scissor;
};

} /* fr */
