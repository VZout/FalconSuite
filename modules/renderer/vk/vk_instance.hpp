#pragma once

#include <vulkan/vulkan.hpp>

#include "../interface.hpp"

namespace fr {

struct _Instance {
	vk::Instance m_native;
	const std::vector<const char*> validation_layers = { "VK_LAYER_LUNARG_standard_validation" };
};

} /* fr */
