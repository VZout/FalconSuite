#include "vk_shader.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "vk_device.hpp"

namespace fr {

void Load(Shader& shader, Device& device, ShaderType type, std::string const& path) {
	vk::Device n_device = device.m_native;

	// load
	std::ifstream file(path, std::ios::ate | std::ios::binary);

	if (!file.is_open()) {
		throw std::runtime_error("failed to open file!");
	}

	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);
	file.seekg(0);
	file.read(buffer.data(), fileSize);
	file.close();

	// compile
	vk::ShaderModuleCreateInfo create_info = {};
	create_info.codeSize = fileSize;
	create_info.pCode = (uint32_t*)buffer.data();

	vk::Result r = n_device.createShaderModule(&create_info, nullptr, &shader.m_module);
	if (r != vk::Result::eSuccess) {
		throw std::runtime_error("Failed to create shader module");
	}
}

void Destroy(Shader& shader, Device& device) {
	vk::Device n_device = device.m_native;
	n_device.destroyShaderModule(shader.m_module);
}

} /* fr */
