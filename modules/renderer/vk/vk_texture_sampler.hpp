#pragma once

#include "../interface.hpp"

#include <vulkan/vulkan.hpp>

namespace fr {

struct _TextureSampler {
	vk::Sampler m_native;
};

} /* fr */
