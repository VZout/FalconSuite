#pragma once

#include "../interface.hpp"

#include <vulkan/vulkan.hpp>

namespace fr {

struct _CommandQueue {
	vk::Queue m_native;
};

} /* fr */

