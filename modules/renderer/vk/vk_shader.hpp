#pragma once

#include "../interface.hpp"

#include <vulkan/vulkan.hpp>

namespace fr {

class RenderSystem;

struct _Shader {
	vk::ShaderModule m_module;
};

} /* fr */
