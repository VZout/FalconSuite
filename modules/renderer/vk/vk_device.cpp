#include "vk_device.hpp"

#include <set>
#include <iostream>

#include "vk_internal.hpp"

namespace fr {

void Create(Device& device, Instance& inst, RenderWindow& render_window) {
#ifdef ENABLE_VK_VALIDATION_LAYERS
	if (!HasValidationLayerSupport(device))
		throw std::runtime_error("Validation layers requested, but not available.");

	InitValidationLayers(device, inst);
#endif

	FindPhysicalDevice(device, inst, render_window);
	device.m_swap_chain_support_details = internal_vk::QuerySwapChainSupport(device.m_physical_device, render_window);

	device.m_queue_family_indices = internal_vk::FindQueueFamilies(device.m_physical_device, render_window);
	std::vector<vk::DeviceQueueCreateInfo> queue_create_infos;
	std::set<int> unique_queue_families = { device.m_queue_family_indices.graphics_family, device.m_queue_family_indices.present_family };
	float queue_priority = 1.0f;

	for (int queue_family : unique_queue_families) {
		vk::DeviceQueueCreateInfo info = {};
		info.queueFamilyIndex = queue_family;
		info.queueCount = 1;
		info.pQueuePriorities = &queue_priority;
		queue_create_infos.push_back(info);
	}

	vk::PhysicalDeviceFeatures device_features = {};

	vk::DeviceCreateInfo create_info = {};
	device_features.wideLines = true; //TODO: This can cause problems on some hardware (mobile?) or impact performance.
	create_info.pQueueCreateInfos = queue_create_infos.data();
	create_info.queueCreateInfoCount = queue_create_infos.size();
	create_info.pEnabledFeatures = &device_features;
	create_info.enabledExtensionCount = device.m_device_extensions.size();
	create_info.ppEnabledExtensionNames = device.m_device_extensions.data();
#ifdef ENABLE_VK_VALIDATION_LAYERS
	create_info.enabledLayerCount = device.m_validation_layers.size();
	create_info.ppEnabledLayerNames = device.m_validation_layers.data();
#else
	device_create_info.enabledLayerCount = 0;
#endif

	vk::Result r = device.m_physical_device.createDevice(&create_info, nullptr, &device.m_native);
	if (r != vk::Result::eSuccess) {
		throw std::runtime_error("Failed to create logical device");
	}

	/* Create The Command Pool */
	vk::Device n_device = device.m_native;

	vk::CommandPoolCreateInfo pool_info = {};
	pool_info.queueFamilyIndex = device.m_queue_family_indices.graphics_family;
	pool_info.flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer; // Optional

	r = n_device.createCommandPool(&pool_info, nullptr, &device.m_cmd_pool);
	if (r != vk::Result::eSuccess) {
		throw std::runtime_error("Failed to create command pool.");
	}
}

#ifdef ENABLE_VK_VALIDATION_LAYERS
void DestroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback, const VkAllocationCallbacks* pAllocator) {
	auto func = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
	if (func != nullptr) {
		func(instance, callback, pAllocator);
	}
}
#endif

// TODO: Maybe move debug report to instance?
void Destroy(Device& device, Instance& inst) {
	vk::Device n_device = device.m_native;
	vk::Instance n_inst = inst.m_native;
#ifdef ENABLE_VK_VALIDATION_LAYERS
	DestroyDebugReportCallbackEXT((VkInstance)n_inst, device.m_debug_callback, nullptr);
#endif
	n_device.destroyCommandPool(device.m_cmd_pool);
	n_device.destroy();
}

void Query(Device& device, Instance& inst, RenderWindow& render_window) {
	device.m_swap_chain_support_details = internal_vk::QuerySwapChainSupport(device.m_physical_device, render_window);

	device.m_queue_family_indices = internal_vk::FindQueueFamilies(device.m_physical_device, render_window);
	std::vector<vk::DeviceQueueCreateInfo> queue_create_infos;
	std::set<int> unique_queue_families = { device.m_queue_family_indices.graphics_family, device.m_queue_family_indices.present_family };
	float queue_priority = 1.0f;

	for (int queue_family : unique_queue_families) {
		vk::DeviceQueueCreateInfo info = {};
		info.queueFamilyIndex = queue_family;
		info.queueCount = 1;
		info.pQueuePriorities = &queue_priority;
		queue_create_infos.push_back(info);
	}
}


void FindPhysicalDevice(Device& device, Instance& inst, RenderWindow& render_window) {
	vk::PhysicalDevice physical_device;
	uint32_t num_devices = 0;

	// only to get the amount of devices.
	inst.m_native.enumeratePhysicalDevices(&num_devices, nullptr);

	if (num_devices == 0) {
		throw std::runtime_error("Failed to find a suitable GPU");
	}

	std::vector<vk::PhysicalDevice> devices(num_devices);
	static_cast<vk::Instance>(inst.m_native).enumeratePhysicalDevices(&num_devices, devices.data());

	for (const auto& d : devices) {
		if (internal_vk::HasDeviceExtensionSupport(d, device.m_device_extensions) && internal_vk::IsSuitablePhysicalDevice(d, render_window)) {
			physical_device = d;
			break;
		}
	}

	if (!physical_device) {
		throw std::runtime_error("Failed to find a suitable GPU");
	}

	device.m_physical_device = physical_device;
}

bool HasValidationLayerSupport(Device& device) {
	uint32_t num_layers;
	vkEnumerateInstanceLayerProperties(&num_layers, nullptr);

	std::vector<VkLayerProperties> layers(num_layers);
	vkEnumerateInstanceLayerProperties(&num_layers, layers.data());

	for (const char* layer_name : device.m_validation_layers) {
		bool found_layer = false;

		for (const auto& layerProperties : layers) {
			if (strcmp(layer_name, layerProperties.layerName) == 0) {
				found_layer = true;
				break;
			}
		}

		if (!found_layer) {
			return false;
		}
	}

	return true;
}

Format FindDepthFormat(Device& device) {
	vk::Format retval = internal_vk::FindSupportedFormat(device,
		{ vk::Format::eD32Sfloat, vk::Format::eD32SfloatS8Uint, vk::Format::eD24UnormS8Uint },
		vk::ImageTiling::eOptimal,
		vk::FormatFeatureFlagBits::eDepthStencilAttachment
	);

	return ToFR(retval);
}

#ifdef ENABLE_VK_VALIDATION_LAYERS
VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(
	VkDebugReportFlagsEXT flags,
	VkDebugReportObjectTypeEXT objType,
	uint64_t obj,
	size_t location,
	int32_t code,
	const char* layerPrefix,
	const char* msg,
	void* userData) {

	std::cerr << "validation layer: " << msg << "\n\n";

	return VK_FALSE;
};

void InitValidationLayers(Device& device, Instance& inst) {
	VkDebugReportCallbackCreateInfoEXT create_info = {};
	create_info.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
	create_info.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
	create_info.pfnCallback = DebugCallback;

	// Dynamically load debugging function.
	PFN_vkCreateDebugReportCallbackEXT CreateDebugReportCallback = VK_NULL_HANDLE;
	CreateDebugReportCallback = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr((VkInstance)inst.m_native, "vkCreateDebugReportCallbackEXT"); // FIXME: instance CAST

	VkResult r = CreateDebugReportCallback((VkInstance)inst.m_native, &create_info, nullptr, &device.m_debug_callback); // FIXME: instance CAST
	if (r != VK_SUCCESS) {
		throw std::runtime_error("Failed to create debug report callback");
	}
}
#endif

} /* fr */
