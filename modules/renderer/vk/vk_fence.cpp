#include "vk_fence.hpp"

#include "vk_internal.hpp"

namespace fr {

void Create(Fence& fence, Device& device) {
	vk::FenceCreateInfo fence_info;
	fence.m_native = device.m_native.createFence(fence_info);
}

void Destroy(Fence& fence, Device& device) {
	device.m_native.destroyFence(fence.m_native);
}

void WaitFor(Fence& fence, Device& device) {
	vk::Device n_device = device.m_native;

	vk::Result r = n_device.waitForFences(1, &fence.m_native, true, std::numeric_limits<uint64_t>::max());
	if (r == vk::Result::eSuccess) {
		n_device.resetFences(1, &fence.m_native);
	}
}

} /* fr */
