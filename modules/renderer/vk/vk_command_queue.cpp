#include "vk_command_queue.hpp"

namespace fr {

void Create(CommandQueue& cmd_queue, Device& device, CmdQueueType type) {
	vk::Device n_device = device.m_native;

	int queue_family = -1;
	switch (type) {
	case CmdQueueType::DIRECT:
		queue_family = device.m_queue_family_indices.present_family;
		break;
	case CmdQueueType::COMPUTE:
		std::cerr << "WARNING > UNSUPORTED COMMAND QUEUE TYPE" << std::endl;
		queue_family = device.m_queue_family_indices.present_family;
		break;
	case CmdQueueType::COPY:
		queue_family = device.m_queue_family_indices.graphics_family;
		break;
	default:
		throw std::runtime_error("Unknown cmd queue type");
		break;
	}

	// TODO: Increment index.
	n_device.getQueue(queue_family, 0, &cmd_queue.m_native);
}

void Execute(CommandQueue& cmd_queue, std::vector<CommandList> cmd_lists, ExecuteInfo info) {
	std::vector<vk::CommandBuffer> buffers;
	for (auto i = 0; i < cmd_lists.size(); i++) {
		buffers.push_back(cmd_lists[i].m_native[cmd_lists[i].m_current_frame_idx]);
	}

	vk::SubmitInfo submit_info = {};
	submit_info.commandBufferCount = cmd_lists.size();
	submit_info.pCommandBuffers = buffers.data();

	if (info.wait_stages) {
		submit_info.pWaitDstStageMask = info.wait_stages;
	}

	if (info.fence)
		cmd_queue.m_native.submit(1, &submit_info, info.fence->m_native);
	else
		cmd_queue.m_native.submit(1, &submit_info, nullptr);
}

void WaitIdle(CommandQueue& cmd_queue) {
	cmd_queue.m_native.waitIdle();
}

} /* fr */

