#include "vk_buffer.hpp"

#include "vk_device.hpp"
#include "vk_internal.hpp"

namespace fr {

void Create(Buffer& buffer, Device& device, uint64_t size, unsigned char usage_flags, int properties) {
	vk::Device n_device = device.m_native;

	vk::BufferCreateInfo buffer_info = {};
	buffer_info.size = size;
	buffer_info.usage = vk::BufferUsageFlagBits(usage_flags);
	buffer_info.sharingMode = vk::SharingMode::eExclusive;

	if (n_device.createBuffer(&buffer_info, nullptr, &buffer.m_native) != vk::Result::eSuccess) {
		throw std::runtime_error("failed to create buffer!");
	}

	vk::MemoryRequirements mem_requirements;
	n_device.getBufferMemoryRequirements(buffer.m_native, &mem_requirements);

	vk::MemoryAllocateInfo alloc_info = {};
	alloc_info.allocationSize = mem_requirements.size;
	alloc_info.memoryTypeIndex = internal_vk::FindMemoryType(device, mem_requirements.memoryTypeBits, vk::MemoryPropertyFlagBits(properties));

	if (n_device.allocateMemory(&alloc_info, nullptr, &buffer.m_memory) != vk::Result::eSuccess) {
		throw std::runtime_error("failed to allocate buffer memory!");
	}

	n_device.bindBufferMemory(buffer.m_native, buffer.m_memory, 0);
}

void Destroy(Buffer& buffer, Device& device) {
	vk::Device n_device = device.m_native;

	n_device.destroyBuffer(buffer.m_native);
	n_device.freeMemory(buffer.m_memory);
}

void CopyBuffer(CommandList& cmd_list, Buffer& src, Buffer& dest, uint64_t size) {
	vk::CommandBuffer n_cmd_list = cmd_list.m_native[cmd_list.m_current_frame_idx];

	vk::BufferCopy copyRegion = {};
	copyRegion.size = size;
	n_cmd_list.copyBuffer(src.m_native, dest.m_native, 1, &copyRegion);
}

} /* fr */
