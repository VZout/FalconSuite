#pragma once

#include "../interface.hpp"

#include <vulkan/vulkan.hpp>

namespace fr {

struct _CommandList {
	vk::CommandBuffer* m_native;
	unsigned int m_num;
	unsigned int m_current_frame_idx;
};

} /* fr */
