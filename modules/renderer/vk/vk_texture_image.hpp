#pragma once

#include <vulkan\vulkan.hpp>
#include "../interface.hpp"
#include "../defines.hpp"
#include <array>

namespace fr {

struct ImgData {
	int m_width;
	int m_height;
	int m_channels;
	VkDeviceSize m_size;
	unsigned char* m_pixel_data;
};

struct _TextureArray {
	std::vector<vk::Image> staging_img;
	std::vector<vk::DeviceMemory> staging_img_mem;

	std::vector<ImgData> m_img_data;
	//std::vector<vk::Image> _texture_image;
	//std::vector<vk::DeviceMemory> _texture_image_mem;
	//std::vector<vk::ImageView> _texture_image_view;

	std::vector<Resource> m_resources;

	vk::DescriptorSet m_desc_set;
};

} /* fr */
