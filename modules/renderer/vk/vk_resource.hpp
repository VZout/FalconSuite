#pragma once

#include "../interface.hpp"

namespace fr {

struct _Resource {
	vk::Image m_image;
	vk::DeviceMemory m_memory;
	vk::ImageView m_view;
};

} /* fr */