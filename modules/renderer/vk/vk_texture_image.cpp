#include "vk_texture_image.hpp"

#include <vulkan/vulkan.hpp>

#include "vk_internal.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

namespace fr {

void Load(TextureArray& texture_array, std::vector<std::string> paths) {
	for (auto i = 0; i < paths.size(); i++) {
		ImgData data;
		data.m_pixel_data = stbi_load(paths[i].c_str(), &data.m_width, &data.m_height, &data.m_channels, STBI_rgb_alpha);
		data.m_size = data.m_width * data.m_height * 4;

		if (!data.m_pixel_data) {
			throw std::runtime_error("STB Failed to load texture image.");
		}

		texture_array.m_img_data.push_back(data);
	}
}

void Destroy(TextureArray& texture_array, RootSignature& root_signature, Device& device) {
	vk::Device n_device = device.m_native;

	for (auto i = 0; i < texture_array.m_resources.size(); i++) {
		Destroy(texture_array.m_resources[i], device);
		if (texture_array.staging_img[i]) n_device.destroyImage(texture_array.staging_img[i]);
		if (texture_array.staging_img_mem[i]) n_device.freeMemory(texture_array.staging_img_mem[i]);
	}

	vk::DescriptorSet sets[1] = { texture_array.m_desc_set };
	n_device.freeDescriptorSets(root_signature.m_desc_pool, 1, sets);
}

void CreateImages(CommandList* cmd_list, TextureArray& texture_array, Device& device) {
	vk::Device n_device = device.m_native;

	texture_array.m_resources.resize(texture_array.m_img_data.size());
	texture_array.staging_img.resize(texture_array.m_img_data.size());
	texture_array.staging_img_mem.resize(texture_array.m_img_data.size());
	for (auto i = 0; i < texture_array.m_resources.size(); i++) {

		ImgData data = texture_array.m_img_data[i];

		// Create vk staging image and stage
		internal_vk::CreateImage(device, data.m_width, data.m_height, vk::Format::eR8G8B8A8Unorm,
			vk::ImageTiling::eLinear,
			vk::ImageUsageFlagBits::eTransferSrc,
			vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent,
			texture_array.staging_img[i], texture_array.staging_img_mem[i]);

		vk::ImageSubresource subresource = {};
		subresource.aspectMask = vk::ImageAspectFlagBits::eColor;
		subresource.mipLevel = 0;
		subresource.arrayLayer = 0;

		vk::SubresourceLayout staging_img_layout;
		n_device.getImageSubresourceLayout(texture_array.staging_img[i], &subresource, &staging_img_layout);

		void* temp_data;
		n_device.mapMemory(texture_array.staging_img_mem[i], 0, data.m_size, vk::MemoryMapFlagBits(0), &temp_data);

		if (staging_img_layout.rowPitch == data.m_width * 4) {
			memcpy(temp_data, data.m_pixel_data, (size_t)data.m_size);
		}
		else {
			uint8_t* dataBytes = reinterpret_cast<uint8_t*>(temp_data);

			for (int y = 0; y < data.m_height; y++) {
				memcpy(&dataBytes[y * staging_img_layout.rowPitch], &data.m_pixel_data[y * data.m_width * 4], data.m_width * 4);
			}
		}

		n_device.unmapMemory(texture_array.staging_img_mem[i]);

		stbi_image_free(data.m_pixel_data);

		// Create Resources

		ResourceCreateInfo create_info;
		create_info.tiling = ImageTiling::OPTIMAL;
		create_info.usage = ImageUsage::SAMPLED | ImageUsage::TRANSFER_DST;
		create_info.mem_properties = MemoryProperties::DEVICE_LOCAL;
		create_info.format = Format::R8G8B8A8_UNORM;
		create_info.width = data.m_width;
		create_info.height = data.m_height;
		create_info.aspect = ImageAspect::COLOR;
		Create(texture_array.m_resources[i], device, create_info);

		// Prep image
		vk::CommandBuffer n_cmd_list = cmd_list->m_native[cmd_list->m_current_frame_idx];
		internal_vk::TransitionImageLayout(*cmd_list, texture_array.staging_img[i], Format::R8G8B8A8_UNORM, ResourceState::GENERAL_READ, ResourceState::COPY_SOURCE);
		TransitionImageLayout(*cmd_list, texture_array.m_resources[i], Format::R8G8B8A8_UNORM, ResourceState::GENERAL_READ, ResourceState::COPY_DEST);
		internal_vk::CopyImage(n_cmd_list, texture_array.staging_img[i], texture_array.m_resources[i].m_image, data.m_width, data.m_height);
	}
}

void Transition(CommandList& cmd_list, TextureArray& texture_array, ResourceState old_state, ResourceState new_state) {
	for (unsigned int i = 0; i < texture_array.m_resources.size(); i++) {
		TransitionImageLayout(cmd_list, texture_array.m_resources[i], Format::B8G8R8A8_UNORM, old_state, new_state);																																												 // Not setting it to read only optimal seems to impact performance massivly though... FIXME!!!!!!!
	}
}

void CreateDescriptorSet(TextureArray& texture_array, TextureSampler& sampler, Device& device, RootSignature& root_signature) {
	vk::Device n_device = device.m_native;

	// Allocate descriptor set
	vk::DescriptorSetLayout layouts[] = { root_signature.m_desc_set_layout_img };
	vk::DescriptorSetAllocateInfo alloc_info = {};
	alloc_info.descriptorPool = root_signature.m_desc_pool;
	alloc_info.descriptorSetCount = 1;
	alloc_info.pSetLayouts = layouts;

	vk::Result r = n_device.allocateDescriptorSets(&alloc_info, &texture_array.m_desc_set);
	if (r != vk::Result::eSuccess) {
		throw std::runtime_error("failed to create descriptor set.");
	}

	std::array<vk::DescriptorImageInfo, 3> image_info = {};
	std::array<vk::WriteDescriptorSet, 3> descriptorWrites = {};
	for (auto i = 0; i < descriptorWrites.size(); i++) {

		image_info[i].imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
		image_info[i].imageView = texture_array.m_resources[i].m_view;
		image_info[i].sampler = sampler.m_native;

		descriptorWrites[i].dstSet = texture_array.m_desc_set;
		descriptorWrites[i].dstBinding = 0;
		descriptorWrites[i].dstArrayElement = i;
		descriptorWrites[i].descriptorType = vk::DescriptorType::eCombinedImageSampler;
		descriptorWrites[i].descriptorCount = 1;
		descriptorWrites[i].pImageInfo = &image_info[i];
	}

	n_device.updateDescriptorSets(descriptorWrites.size(), descriptorWrites.data(), 0, nullptr);
}

} /* fr */
