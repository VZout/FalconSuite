#pragma once

#include "../interface.hpp"

namespace fr {

struct _StagingBuffer {
	Buffer* m_buffer;
	Buffer* m_staging;
	uint64_t m_size;
};

struct _UniformStagingBuffer : _StagingBuffer {
	vk::DescriptorSet m_desc_set;
};

} /* fr */
