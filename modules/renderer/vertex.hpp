#pragma once

#include <vulkan/vulkan.hpp>

#include <math\vec.hpp>
#include <functional>

namespace fr {

struct Vertex {
	fm::vec3 m_pos;
	fm::vec3 m_normal;
	fm::vec2 m_texCoord;

	Vertex GetData() {
		Vertex retval{ m_pos, m_normal, m_texCoord };
		return {  };
	}

	static vk::VertexInputBindingDescription GetBindingDescription() {
		vk::VertexInputBindingDescription bindingDescription = {};
		bindingDescription.binding = 0;
		bindingDescription.stride = sizeof(Vertex);
		bindingDescription.inputRate = vk::VertexInputRate::eVertex;

		return bindingDescription;
	}

#ifdef FR_USE_DX12
	static std::vector<D3D12_INPUT_ELEMENT_DESC> GetInputLayout() {
		std::vector<D3D12_INPUT_ELEMENT_DESC> layout = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Vertex, m_pos),      D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Vertex, m_normal),   D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Vertex, m_texCoord), D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		};

		return layout;
	}
#endif

	static std::array<vk::VertexInputAttributeDescription, 3> GetAttributeDescriptions() {
		std::array<vk::VertexInputAttributeDescription, 3> attrib_descs = {};

		attrib_descs[0].binding = 0;
		attrib_descs[0].location = 0;
		attrib_descs[0].format = vk::Format::eR32G32B32Sfloat;
		attrib_descs[0].offset = offsetof(Vertex, m_pos);

		attrib_descs[1].binding = 0;
		attrib_descs[1].location = 1;
		attrib_descs[1].format = vk::Format::eR32G32B32Sfloat;
		attrib_descs[1].offset = offsetof(Vertex, m_normal);

		attrib_descs[2].binding = 0;
		attrib_descs[2].location = 2;
		attrib_descs[2].format = vk::Format::eR32G32Sfloat;
		attrib_descs[2].offset = offsetof(Vertex, m_texCoord);

		return attrib_descs;
	}

	bool operator==(const Vertex& other) const {
		return m_pos == other.m_pos && m_normal == other.m_normal && m_texCoord == other.m_texCoord;
	}
};

} /* fr */

namespace std {
	template<> struct hash<fm::vec3> {
		size_t operator()(fm::vec3 const& v) const {
			return ((hash<float>()(v.x) ^
				(hash<float>()(v.y) << 1)) >> 1) ^
				(hash<float>()(v.z) << 1);
		}
	};

	template<> struct hash<fm::vec2> {
		size_t operator()(fm::vec2 const& v) const {
			return ((hash<float>()(v.x) ^
				(hash<float>()(v.y) << 1)) >> 1);
		}
	};

	template<> struct hash<fr::Vertex> {
		size_t operator()(fr::Vertex const& vertex) const {
			return ((hash<fm::vec3>()(vertex.m_pos) ^
				(hash<fm::vec3>()(vertex.m_normal) << 1)) >> 1) ^
				(hash<fm::vec2>()(vertex.m_texCoord) << 1);
		}
	};
}
