#pragma once

#include "interface.hpp"

struct GLFWwindow;

namespace fr {

struct _Window {
	GLFWwindow* m_native;
};

} /* fr */
