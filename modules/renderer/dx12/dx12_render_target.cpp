#include "dx12_render_target.hpp"

#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW\glfw3.h>
#include <GLFW\glfw3native.h>

#include "d3dx12.h"

namespace fr {

void PreInitialize(RenderWindow& render_window, Instance& inst, Window& window) {
	render_window.m_surface = glfwGetWin32Window(window.m_native);
}

void Create(RenderWindow& render_window, Device& device, CommandQueue& queue, int width, int height, std::vector<RenderTargetAttachement> attachements) {
	// Create swapchain
	DXGI_SAMPLE_DESC sample_desc = {};
	sample_desc.Count = 1;
	sample_desc.Quality = 0;

	DXGI_SWAP_CHAIN_DESC1 swap_chain_desc = {};
	swap_chain_desc.Width = width;
	swap_chain_desc.Height = height;
	swap_chain_desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	swap_chain_desc.SampleDesc = sample_desc;
	swap_chain_desc.BufferCount = render_window.m_num_back_buffers; // FIXME: hardcoded
	swap_chain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swap_chain_desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swap_chain_desc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
	swap_chain_desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	IDXGISwapChain1* temp_swap_chain;
	HRESULT hr = device.dxgi_factory->CreateSwapChainForHwnd(
		queue.m_native,
		render_window.m_surface,
		&swap_chain_desc,
		NULL,
		NULL,
		&temp_swap_chain
	);
	if (FAILED(hr)) {
		throw "Failed to create swap chain.";
	}

	render_window.swap_chain = static_cast<IDXGISwapChain4*>(temp_swap_chain);
	render_window.m_frame_idx = (render_window.swap_chain)->GetCurrentBackBufferIndex();

	// Create views
	D3D12_DESCRIPTOR_HEAP_DESC back_buffer_heap_desc = {};
	back_buffer_heap_desc.NumDescriptors = render_window.m_num_back_buffers;
	back_buffer_heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	back_buffer_heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	hr = device.device->CreateDescriptorHeap(&back_buffer_heap_desc, IID_PPV_ARGS(&render_window.rtv_descriptor_heap));
	if (FAILED(hr))
		throw "Failed to create descriptor heap.";

	render_window.rtv_descriptor_increment_size = device.device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

	// Create render target view with the handle to the heap descriptor.
	render_window.render_targets.resize(render_window.m_num_back_buffers);
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtv_handle(render_window.rtv_descriptor_heap->GetCPUDescriptorHandleForHeapStart());
	for (unsigned int i = 0; i < render_window.m_num_back_buffers; i++) {
		hr = render_window.swap_chain->GetBuffer(i, IID_PPV_ARGS(&render_window.render_targets[i]));
		if (FAILED(hr))
			throw "Failed to get swap chain buffer.";

		device.device->CreateRenderTargetView(render_window.render_targets[i], nullptr, rtv_handle);

		rtv_handle.Offset(1, render_window.rtv_descriptor_increment_size);
	}
}

void Present(RenderWindow& render_window, CommandQueue& cmd_queue, Device& device, Fence& fence) {
	render_window.swap_chain->Present(0, 0);
	render_window.m_frame_idx = render_window.swap_chain->GetCurrentBackBufferIndex();

	Signal(fence, cmd_queue);
}

void SetClearColor(RenderWindow& render_window, std::array<float, 4> clear_color) {
	render_window.m_clear_color = clear_color;
}

void Destroy(RenderWindow& render_window, Device& device, Instance& inst) {
}

} /* fr */
