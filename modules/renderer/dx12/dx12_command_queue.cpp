#include "dx12_command_queue.hpp"

namespace fr {

void Create(CommandQueue& cmd_queue, Device& device, CmdQueueType type) {
	D3D12_COMMAND_QUEUE_DESC cmd_queue_desc = {};
	cmd_queue_desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;

	switch (type) {
	case CmdQueueType::DIRECT:
		cmd_queue_desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
		break;
	default:
		cmd_queue_desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
		break;
	}

	HRESULT hr = device.device->CreateCommandQueue(&cmd_queue_desc, IID_PPV_ARGS(&cmd_queue.m_native));
	if (FAILED(hr)) {
		throw "Failed to create direct command queue.";
	}
}


void Execute(CommandQueue& cmd_queue, std::vector<CommandList> cmd_lists, ExecuteInfo info) {
	ID3D12CommandList** lists = new ID3D12CommandList*[cmd_lists.size()];
	for (auto i = 0; i < cmd_lists.size(); i++) {
		lists[i] = cmd_lists[i].cmd_list;
	}

	cmd_queue.m_native->ExecuteCommandLists(1, lists);

	if (info.fence) {
		Signal(*info.fence, cmd_queue);
	}
}

/*
void WaitIdle(CommandQueue& cmd_queue) {

}
*/

} /* fr */

