#pragma once

#include "../interface.hpp"

#include <d3d12.h>

namespace fr {

struct _CommandList {
	ID3D12CommandAllocator** cmd_allocators;
	ID3D12GraphicsCommandList* cmd_list;
};

} /* fr */