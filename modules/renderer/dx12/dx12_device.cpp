#include "dx12_device.hpp"
#include "d3dx12.h"

#include <set>
#include <iostream>

namespace fr {

void Create(Device& device, Instance& inst, RenderWindow& render_window) {
	HRESULT hr = CreateDXGIFactory1(IID_PPV_ARGS(&device.dxgi_factory));
	if (FAILED(hr)) {
		throw "Failed to create DXGIFactory.";
	}

	FindPhysicalDevice(device, inst, render_window);

	// Actually create the device.
	hr = D3D12CreateDevice(device.adapter, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&device.device));
}

#ifdef ENABLE_VK_VALIDATION_LAYERS
void DestroyDebugReportCallbackEXT(VkInstance instance, VkDebugReportCallbackEXT callback, const VkAllocationCallbacks* pAllocator) {
}
#endif

// TODO: Maybe move debug report to instance?
void Destroy(Device& device, Instance& inst) {
}

void Query(Device& device, Instance& inst, RenderWindow& render_window) {
}


void FindPhysicalDevice(Device& device, Instance& inst, RenderWindow& render_window) {
	IDXGIAdapter1* adapter = nullptr;
	int adapterIndex = 0;

	// Find a compatible adapter.
	while ((device.dxgi_factory)->EnumAdapters1(adapterIndex, &adapter) != DXGI_ERROR_NOT_FOUND) {
		DXGI_ADAPTER_DESC1 desc;
		adapter->GetDesc1(&desc);

		// Skip software adapters.
		if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) {
			adapterIndex++;
			continue;
		}

		// Create a device to test if the adapter supports the specified feature level.
		HRESULT hr = D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_11_0, _uuidof(ID3D12Device), nullptr);
		if (SUCCEEDED(hr))
			break;

		adapterIndex++;
	}

	if (adapter == nullptr) {
		throw "No comaptible adapter found.";
	}
	else {
		device.adapter = adapter;
	}
}

bool HasValidationLayerSupport(Device& device) {
	return true;
}

Format FindDepthFormat(Device& device) {
	return Format::B4G4R4A4_UNORM;
}

#ifdef ENABLE_VK_VALIDATION_LAYERS
void InitValidationLayers(Device& device, Instance& inst) {
}
#endif

} /* fr */
