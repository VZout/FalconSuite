#include "dx12_instance.hpp"

#include "wrl.h"
#include <dxgidebug.h>

namespace fr {

	void Create(Instance& inst) {
		Microsoft::WRL::ComPtr<ID3D12Debug> debugController;
		if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController)))) {
			debugController->EnableDebugLayer();
		}
	}

	void Destroy(Instance& inst) {
	}

} /* fr */
