#include "dx12_pipeline_state.hpp"

#include <stdexcept>
#include <iostream>

#include "../vertex.hpp" // FIXME: Should'nt be nessessary.
#include "d3dx12.h"

namespace fr {

void SetVertexShader(PipelineState& pipeline_state, Shader& shader) {
	pipeline_state.m_vertex_shader = shader;
}

void SetFragmentShader(PipelineState& pipeline_state, Shader& shader) {
	pipeline_state.m_pixel_shader = shader;
}

void SetRootSignature(PipelineState& pipeline_state, RootSignature* root_signature) {
	pipeline_state.m_root_signature = root_signature;
}

void Finalize(PipelineState& pipeline_state, Device& device, RenderWindow& render_window) {
D3D12_BLEND_DESC blend_desc = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	D3D12_DEPTH_STENCIL_DESC depth_stencil_state = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	D3D12_RASTERIZER_DESC rasterize_desc = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	DXGI_SAMPLE_DESC sampleDesc = {1, 0};

	pipeline_state.m_input_layout = Vertex::GetInputLayout();

	D3D12_INPUT_LAYOUT_DESC input_layout_desc = {};
	input_layout_desc.NumElements = pipeline_state.m_input_layout.size();
	input_layout_desc.pInputElementDescs = pipeline_state.m_input_layout.data();

	D3D12_SHADER_BYTECODE vs_bytecode = {};
	vs_bytecode.BytecodeLength = pipeline_state.m_vertex_shader.m_native->GetBufferSize();
	vs_bytecode.pShaderBytecode = pipeline_state.m_vertex_shader.m_native->GetBufferPointer();

	D3D12_SHADER_BYTECODE ps_bytecode = {};
	ps_bytecode.BytecodeLength = pipeline_state.m_pixel_shader.m_native->GetBufferSize();
	ps_bytecode.pShaderBytecode = pipeline_state.m_pixel_shader.m_native->GetBufferPointer();

	D3D12_GRAPHICS_PIPELINE_STATE_DESC pso_desc = {};
	pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pso_desc.RTVFormats[0] = DXGI_FORMAT_B8G8R8A8_UNORM;
	//pso_desc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
	pso_desc.SampleDesc = sampleDesc;
	pso_desc.SampleMask = 0xffffffff;
	pso_desc.RasterizerState = rasterize_desc;
	pso_desc.BlendState = blend_desc;
	//pso_desc.DepthStencilState = depth_stencil_state;
	pso_desc.NumRenderTargets = 1;
	pso_desc.pRootSignature = pipeline_state.m_root_signature->m_native;
	pso_desc.VS = vs_bytecode;
	pso_desc.PS = ps_bytecode;
	pso_desc.InputLayout = input_layout_desc;

	HRESULT hr = device.device->CreateGraphicsPipelineState(&pso_desc, IID_PPV_ARGS(&pipeline_state.m_native));
	if (FAILED(hr)) {
		throw "Failed to create graphics pipeline";
	}
	pipeline_state.m_native->SetName(L"My sick pipeline object");
}

void Destroy(PipelineState& pipeline_state, Device& device) {

}

} /* fr */
