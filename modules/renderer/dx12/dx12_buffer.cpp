#include "dx12_buffer.hpp"

#include "d3dx12.h"

namespace fr {

void Create(Buffer& buffer, Device& device, uint64_t size, unsigned char usage_flags, int properties) {
	device.device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(size),
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&buffer.m_native));

	buffer.m_native->SetName(L"Native Buffer");
}

void Destroy(Buffer& buffer, Device& device) {

}

void CopyBuffer(CommandList& cmd_list, Buffer& src, Buffer& dest, uint64_t size) {
	//UpdateSubresources(cmd_list, src.m_native, dest.m_native, 0, 0, 1, &vertex_data);
}

} /* fr */
