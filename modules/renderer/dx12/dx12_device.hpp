#pragma once

#include "../interface.hpp"

#include <d3d12.h>
#include <dxgi1_5.h>

namespace fr {

struct _Device {
	IDXGIAdapter1* adapter;
	ID3D12Device* device;
	IDXGIFactory5* dxgi_factory;
};

} /* fr */
