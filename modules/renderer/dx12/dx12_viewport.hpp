#pragma once

#include "../interface.hpp"

#include <d3d12.h>

namespace fr {

struct _Viewport {
	D3D12_VIEWPORT viewport;
	D3D12_RECT scissor_rect;
};

} /* fr */
