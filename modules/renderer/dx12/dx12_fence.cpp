#include "dx12_fence.hpp"

namespace fr {

void Create(Fence& fence, Device& device) {
	HRESULT hr;

	// create the fences
	hr = device.device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence.fence));
	if (FAILED(hr)) {
		throw "Failed to create fence.";
	}  
	fence.fence_value = 0; // set the initial fence value to 0

	// create a handle to a fence event
	fence.fence_event = CreateEvent(nullptr, FALSE, FALSE, nullptr);
	if (fence.fence_event == nullptr) {
		throw "Failed to create fence event.";
	}
}

void Destroy(Fence& fence, Device& device) {
}

void Signal(Fence& fence, CommandQueue& cmd_queue) {
	HRESULT hr = cmd_queue.m_native->Signal(fence.fence, fence.fence_value);
	if (FAILED(hr)) {
		throw "Failed to set fence signal.";
	}
}

void WaitFor(Fence& fence, Device& device) {
	if (fence.fence->GetCompletedValue() < fence.fence_value) {
		// we have the fence create an event which is signaled once the fence's current value is "fenceValue"
		HRESULT hr = fence.fence->SetEventOnCompletion(fence.fence_value, fence.fence_event);
		if (FAILED(hr)) {
			throw "Failed to set fence event.";
		}

		WaitForSingleObject(fence.fence_event, INFINITE);
	}

	// increment fenceValue for next frame
	fence.fence_value++;
}

} /* fr */
