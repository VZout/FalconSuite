#include "dx12_staging_buffer.hpp"

#include "d3dx12.h"
#include "../vertex.hpp"

namespace fr {

void Create(StagingBuffer& buffer, Device& device, void* data, uint64_t size, unsigned char usage_flags, int properties) {
	buffer.m_size = size;

	buffer.m_stride_in_bytes = sizeof(Vertex);

	device.device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(size),
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&buffer.m_buffer));
	buffer.m_buffer->SetName(L"Vertex Buffer Resource Heap");

	device.device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(size),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&buffer.m_staging));
	buffer.m_staging->SetName(L"Vertex Buffer Upload Resource Heap");
	
	buffer.m_data = data;
}

void Update(StagingBuffer& buffer, Device& device, vk::DeviceSize size, void* data) {

}

void StageBuffer(StagingBuffer& buffer, CommandList& cmd_list) {
	// store vertex buffer in upload heap
	D3D12_SUBRESOURCE_DATA vertex_data = {};
	vertex_data.pData = reinterpret_cast<BYTE*>(buffer.m_data);
	vertex_data.RowPitch = buffer.m_size;
	vertex_data.SlicePitch = buffer.m_size;

	UpdateSubresources(cmd_list.cmd_list, buffer.m_buffer, buffer.m_staging, 0, 0, 1, &vertex_data);

	// transition the vertex buffer data from copy destination state to vertex buffer state
	cmd_list.cmd_list->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(buffer.m_buffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER));
}

void FreeStagingBuffer(StagingBuffer& buffer, Device& device) {

}

void Destroy(StagingBuffer& buffer, Device& device) {

}

void Create(UniformStagingBuffer& buffer, RootSignature& root_signature, Device& device, void* data, uint64_t size, unsigned char usage_flags, int properties) {

}

void Destroy(UniformStagingBuffer& buffer, RootSignature& root_signature, Device& device) {

}

} /* fr */
