#pragma once

#include "../interface.hpp"

namespace fr {

struct _StagingBuffer {
	ID3D12Resource* m_buffer;
	ID3D12Resource* m_staging;
	D3D12_VERTEX_BUFFER_VIEW m_view;
	uint64_t m_size;
	uint64_t m_stride_in_bytes;
	void* m_data;
};

struct _UniformStagingBuffer : _StagingBuffer {
	
};

} /* fr */
