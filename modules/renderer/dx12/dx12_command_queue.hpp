#pragma once

#include "../interface.hpp"

#include <d3d12.h>

namespace fr {

struct _CommandQueue {
	ID3D12CommandQueue* m_native;
};

} /* fr */

