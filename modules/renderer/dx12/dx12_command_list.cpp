#include "dx12_command_list.hpp"

#include "d3dx12.h"

namespace fr {

void Allocate(CommandList& cmd_list, Device& device, unsigned int num) {
	cmd_list.cmd_allocators = new ID3D12CommandAllocator*[num];

	// Create the allocators
	for (int i = 0; i < num; i++) {
		HRESULT hr = device.device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&cmd_list.cmd_allocators[i]));
		if (FAILED(hr)) {
			throw "Failed to create command allocator";
		}

		cmd_list.cmd_allocators[i]->SetName(L"CommandList allocator.");
	}

	// Create the command lists
	HRESULT hr = device.device->CreateCommandList(
		0,
		D3D12_COMMAND_LIST_TYPE_DIRECT,
		cmd_list.cmd_allocators[0],
		NULL,
		IID_PPV_ARGS(&cmd_list.cmd_list)
	);
	if (FAILED(hr)) {
		throw "Failed to create command list";
	}

	cmd_list.cmd_list->SetName(L"Native Commandlist");

	cmd_list.cmd_list->Close();
}

/*
void Destroy(CommandList& cmd_list, Device& device) {
	vk::Device n_device = device.m_native;

	vk::CommandBuffer buffers[1] = { cmd_list.m_native };
	n_device.freeCommandBuffers(device.m_cmd_pool, 1, buffers);
}
*/

void Begin(CommandList& cmd_list, RenderWindow& render_window, bool temp) {
	// TODO: move resetting to when the command list is executed. This is how vulkan does it.
	HRESULT hr = cmd_list.cmd_allocators[render_window.m_frame_idx]->Reset();
	if (FAILED(hr)) {
		throw "Failed to reset cmd allocators";
	}

	// Only reset with pipeline state if using bundles since only then this will impact fps.
	// Otherwise its just easier to pass NULL and suffer the insignificant performance loss.
	hr = cmd_list.cmd_list->Reset(cmd_list.cmd_allocators[render_window.m_frame_idx], NULL);

	if (FAILED(hr)) {
		throw "Failed to reset command list";
	}

	// Begin the command list.
	CD3DX12_RESOURCE_BARRIER begin_transition = CD3DX12_RESOURCE_BARRIER::Transition(
		render_window.render_targets[render_window.m_frame_idx],
		D3D12_RESOURCE_STATE_PRESENT,
		D3D12_RESOURCE_STATE_RENDER_TARGET
	);
	cmd_list.cmd_list->ResourceBarrier(1, &begin_transition);

	CD3DX12_CPU_DESCRIPTOR_HANDLE rtv_handle(render_window.rtv_descriptor_heap->GetCPUDescriptorHandleForHeapStart(), render_window.m_frame_idx, render_window.rtv_descriptor_increment_size);
	cmd_list.cmd_list->OMSetRenderTargets(1, &rtv_handle, false, nullptr);
	cmd_list.cmd_list->ClearRenderTargetView(rtv_handle, render_window.m_clear_color.data(), 0, nullptr);
}

void End(CommandList& cmd_list, RenderWindow& render_window, bool temp) {
	// Close and transition the cmd list
	CD3DX12_RESOURCE_BARRIER end_transition = CD3DX12_RESOURCE_BARRIER::Transition(
		render_window.render_targets[render_window.m_frame_idx],
		D3D12_RESOURCE_STATE_RENDER_TARGET,
		D3D12_RESOURCE_STATE_PRESENT
	);

	cmd_list.cmd_list->ResourceBarrier(1, &end_transition);
	cmd_list.cmd_list->Close();
}


void Bind(CommandList& cmd_list, Viewport& viewport) {
	cmd_list.cmd_list->RSSetViewports(1, &viewport.viewport); // set the viewports
	cmd_list.cmd_list->RSSetScissorRects(1, &viewport.scissor_rect); // set the scissor rects
}

void Bind(CommandList& cmd_list, PipelineState& pipeline_state) {
	cmd_list.cmd_list->SetPipelineState(pipeline_state.m_native);
	cmd_list.cmd_list->SetGraphicsRootSignature(pipeline_state.m_root_signature->m_native);
}

void BindVertexBuffer(CommandList& cmd_list, StagingBuffer& buffer) {
	D3D12_VERTEX_BUFFER_VIEW view;
	view.BufferLocation = buffer.m_buffer->GetGPUVirtualAddress();
	view.StrideInBytes = buffer.m_stride_in_bytes;
	view.SizeInBytes = buffer.m_size;
	cmd_list.cmd_list->IASetVertexBuffers(0, 1, &view); // set the vertex buffer (using the vertex buffer view)
}

void Draw(CommandList& cmd_list, unsigned int vertex_count, unsigned int inst_count) {
	cmd_list.cmd_list->DrawInstanced(vertex_count, inst_count, 0, 0); // finally draw 3 vertices (draw the triangle)
}
/*
void BindIndexBuffer(CommandList& cmd_list, StagingBuffer& buffer, unsigned int offset) {
	cmd_list.m_native.bindIndexBuffer(buffer.m_buffer->m_native, offset, vk::IndexType::eUint16);
}

void DrawIndexed(CommandList& cmd_list, unsigned int idx_count, unsigned int inst_count) {
	cmd_list.m_native.drawIndexed(idx_count, inst_count, 0, 0, 0);
}
*/

} /* fr */
