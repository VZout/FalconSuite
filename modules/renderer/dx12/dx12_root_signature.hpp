#pragma once

#include "../interface.hpp"
#include <d3d12.h>

namespace fr {

struct _RootSignature {
	unsigned int m_num_constant_buffers;
	ID3D12RootSignature* m_native;
};

} /* fr */
