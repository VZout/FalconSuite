#pragma once

#include <d3d12.h>

namespace fr {

struct _Shader {
	ID3DBlob* m_native;
};

} /* fr */
