#pragma once

#include "../interface.hpp"

#include <d3d12.h>
#include <dxgi1_5.h>

namespace fr {

	struct _RenderWindow {

		const unsigned int m_num_back_buffers = 3;
		unsigned int m_frame_idx = 0;

		HWND m_surface;
		IDXGISwapChain4* swap_chain;
		std::vector<ID3D12Resource*> render_targets;
		ID3D12DescriptorHeap* rtv_descriptor_heap;
		unsigned int rtv_descriptor_increment_size;

		std::array<float, 4> m_clear_color = { 0, 0, 0, 1};
	};
	
} /* fr  */ 
