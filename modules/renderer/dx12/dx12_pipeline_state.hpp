#pragma once

#include "../interface.hpp"

#include "../defines.hpp"
#include <d3d12.h>
#include <memory>

namespace fr {

	struct _PipelineState {
		ID3D12PipelineState* m_native;
		RootSignature* m_root_signature;
		Shader m_vertex_shader;
		Shader m_pixel_shader;
		std::vector<D3D12_INPUT_ELEMENT_DESC> m_input_layout;
	};

} /* fr */
