#pragma once

#include <d3d12.h>

#include "../interface.hpp"

namespace fr {

struct _Fence {
	ID3D12Fence* fence;
	HANDLE fence_event;
	UINT64 fence_value;
};

} /* fr */
