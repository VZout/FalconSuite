#pragma once

#include "../interface.hpp"

#include <d3d12.h>

namespace fr {

	struct _Buffer {
		ID3D12Resource* m_native;
	};

} /* fr */
