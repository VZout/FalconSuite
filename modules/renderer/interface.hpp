#pragma once

#include "defines.hpp"
#include "math\vec.hpp"

#include <memory>
#include <functional>

#include <vulkan/vulkan.hpp>

#include "enums.hpp"

namespace fr {

	// Unconverted Forward Declerations
	class RenderSystem;
	struct Vertex;
	struct WindowCreateInfo;

	// API Declared Structures
	DEC_HANDLE(Instance)
	DEC_HANDLE(Device);
	DEC_HANDLE(RootSignature)
	DEC_HANDLE(CommandList)
	DEC_HANDLE(CommandQueue)
	DEC_HANDLE(PipelineState)
	DEC_HANDLE(Shader)
	DEC_HANDLE(Window)
	DEC_HANDLE(Buffer)
	DEC_HANDLE(StagingBuffer)
	DEC_HANDLE(UniformStagingBuffer)
	DEC_HANDLE(TextureArray)
	DEC_HANDLE(Resource)
	DEC_HANDLE(RenderTarget)
	DEC_HANDLE(RenderWindow)
	DEC_HANDLE(Viewport)
	DEC_HANDLE(TextureSampler)
	DEC_HANDLE(Fence)

	// Instance
	void Create(Instance& inst);
	void Destroy(Instance& inst);

	// Device
	struct QueueFamilyIndices {
		int graphics_family = -1;
		int present_family = -1;

		bool IsComplete() {
			return graphics_family >= 0
				&& present_family >= 0;
		}
	};

	struct SwapChainSupportDetails {
		vk::SurfaceCapabilitiesKHR capabilities;
		std::vector<vk::SurfaceFormatKHR> formats;
		std::vector<vk::PresentModeKHR> present_modes;
	};

	void Create(Device& device, Instance& inst, RenderWindow& render_window);
	void Destroy(Device& device, Instance& inst);
	void Query(Device& device, Instance& inst, RenderWindow& render_window);
	void FindPhysicalDevice(Device& device, Instance& inst, RenderWindow& render_window);
	bool HasValidationLayerSupport(Device& device);
	void InitValidationLayers(Device& device, Instance& inst);
	Format FindDepthFormat(Device& device);

	// Root Signature
	struct RootSignatureCreateInfo {
		int num_const_buffers;
	};

	void Create(RootSignature& root_signature, RootSignatureCreateInfo create_info);
	void Destroy(RootSignature& root_signature, Device& device);
	void Finalize(RootSignature& root_signature, Device& device);

	// Pipeline
	void SetVertexShader(PipelineState& pipeline_state, Shader& shader);
	void SetFragmentShader(PipelineState& pipeline_state, Shader& shader);
	void SetRootSignature(PipelineState& pipeline_state, RootSignature* root_signature);
	void Finalize(PipelineState& pipeline_state, Device& device, RenderWindow& render_window);
	void Destroy(PipelineState& pipeline_state, Device& device);

	// Command List
	void Allocate(CommandList& cmd_list, Device& device, unsigned int num = 1);
	void Destroy(CommandList& cmd_list, Device& device);
	void Begin(CommandList& cmd_list, RenderWindow& render_window, bool temp = false);
	void End(CommandList& cmd_list, RenderWindow& render_window, bool temp = false);
	void Bind(CommandList& cmd_list, Viewport& viewport);
	void Bind(CommandList& cmd_list, PipelineState& pipeline_state);
	void BindVertexBuffer(CommandList& cmd_list, StagingBuffer& buffer);
	void BindIndexBuffer(CommandList& cmd_list, StagingBuffer& buffer, unsigned int offset = 0);
	void Draw(CommandList& cmd_list, unsigned int vertex_count, unsigned int inst_count);
	void DrawIndexed(CommandList& cmd_list, unsigned int idx_count, unsigned int inst_count);

	// Command Queue
	struct ExecuteInfo {
		Fence* fence;
		vk::PipelineStageFlags* wait_stages;
	};

	void Create(CommandQueue& cmd_queue, Device& device, CmdQueueType type);
	void Execute(CommandQueue& cmd_queue, std::vector<CommandList> cmd_lists, ExecuteInfo info = {});
	void WaitIdle(CommandQueue& cmd_queue);

	// Shader
	void Load(Shader& shader, Device& device, ShaderType type, std::string const & path);
	void Destroy(Shader& shader, Device& device);

	// Window
	struct WindowCreateInfo {
		int width = 1280;
		int height = 720;
		std::string title = "Unnamed Application";
		bool fullscreen = false;
	};

	void Create(Window& window, WindowCreateInfo const& create_info);
	void Destroy(Window& window);
	void BindOnResize(Window& window, std::function<void(int width, int height)> callback);
	void SwapBuffers(Window& window);
	void PollEvents();
	void Close(Window& window);
	bool ShouldClose(Window& window);
	const char** GetRequiredInstanceExtensions(unsigned int* count);

	// Buffer
	void Create(Buffer& buffer, Device& device, uint64_t size, unsigned char usage_flags, int properties);
	void Destroy(Buffer& buffer, Device& device);
	void CopyBuffer(CommandList& cmd_list, Buffer& src, Buffer& dest, uint64_t size);

	// StagingBuffer
	void Create(StagingBuffer& buffer, Device& device, void* data, uint64_t size, unsigned char usage_flags, int properties);
	void Destroy(StagingBuffer& buffer, Device& device);
	void Update(StagingBuffer& buffer, Device& device, vk::DeviceSize size, void* data);
	void StageBuffer(StagingBuffer& buffer, CommandList& cmd_list);
	void FreeStagingBuffer(StagingBuffer& buffer, Device& device);

	// Uniform Staging Buffer
	void Create(UniformStagingBuffer& buffer, RootSignature& root_signature, Device& device, void* data, uint64_t size, unsigned char usage_flags, int properties);
	void Destroy(UniformStagingBuffer& buffer, RootSignature& root_signature, Device& device);

	// TextureImageArray
	void Load(TextureArray& texture_array, std::vector<std::string> paths);
	void Destroy(TextureArray& texture_array, RootSignature& root_signature, Device& device);
	void CreateImages(CommandList* cmd_list, TextureArray& texture_array, Device& device);
	void CreateDescriptorSet(TextureArray& texture_array, TextureSampler& sampler, Device& device, RootSignature& root_signature);
	void Transition(CommandList& cmd_list, TextureArray& texture_array, ResourceState old_state, ResourceState new_state);

	// Resource
	struct ResourceCreateInfo {
		int width;
		int height;
		ImageTiling tiling;
		int usage;
		MemoryProperties mem_properties;
		Format format;
		ImageAspect aspect;
	};

	void Create(Resource& resource, Device& device, ResourceCreateInfo const& create_info);
	void Destroy(Resource& resource, Device& device);
	void TransitionImageLayout(CommandList& cmd_list, Resource& resource, Format format, ResourceState old_state, ResourceState new_state);

	// Render Target
	enum RenderTargetAttachementType {
		DEPTH_ATTACHMENT,
	};

	struct RenderTargetAttachement {
		RenderTargetAttachementType type;
		Format format;
		Resource* resource;
	};

	void Create(RenderTarget& render_target, Device& device, std::vector<RenderTargetAttachement> attachements, std::vector<vk::Image> swap_chain_images, Format format);
	void Destroy(RenderTarget& render_target, Device& device);

	// Viewport
	void Create(Viewport& viewport, int width, int height);

	// RenderWindow
	void PreInitialize(RenderWindow& render_window, Instance& inst, Window& window);
	void Create(RenderWindow& render_window, Device& device, CommandQueue& queue, int width, int height, std::vector<RenderTargetAttachement> attachements);
	void Present(RenderWindow& render_window, CommandQueue& cmd_queue, Device& device, Fence& fence);
	void SetClearColor(RenderWindow& render_window, std::array<float, 4> clear_color); // TODO: move to render target instead of render window.
	void Destroy(RenderWindow& render_window, Device& device, Instance& inst);

	// Texture Sampler
	struct TextureSamplerCreateInfo {
		vk::SamplerAddressMode mode = vk::SamplerAddressMode::eRepeat;
	};
	void Create(TextureSampler& sampler, Device& device, TextureSamplerCreateInfo& info = *new TextureSamplerCreateInfo()); //FIXME: *new?
	void Destroy(TextureSampler& sampler, Device& device);

	// Fence
	void Create(Fence& fence, Device& device);
	void Destroy(Fence& fence, Device& device);
	void Signal(Fence& fence, CommandQueue& cmd_queue);
	void WaitFor(Fence& fence, Device& device);

} /* fr */

#ifdef FR_USE_VULKAN
#include "vk/vk_device.hpp"
#include "vk/vk_root_signature.hpp"
#include "vk/vk_command_list.hpp"
#include "vk/vk_command_queue.hpp"
#include "vk/vk_shader.hpp"
#include "vk/vk_pipeline_state.hpp"
#include "vk/vk_buffer.hpp"
#include "vk/vk_staging_buffer.hpp"
#include "vk/vk_resource.hpp"
#include "vk/vk_texture_image.hpp"
#include "vk/vk_render_target.hpp"
#include "vk/vk_viewport.hpp"
#include "vk/vk_texture_sampler.hpp"
#include "vk/vk_instance.hpp"
#include "vk/vk_fence.hpp"
#endif
#ifdef FR_USE_DX12
#include "dx12/dx12_instance.hpp"
#include "dx12/dx12_device.hpp"
#include "dx12/dx12_shader.hpp"
#include "dx12/dx12_render_target.hpp"
#include "dx12/dx12_command_queue.hpp"
#include "dx12/dx12_command_list.hpp"
#include "dx12/dx12_fence.hpp"
#include "dx12/dx12_viewport.hpp"
#include "dx12/dx12_root_signature.hpp"
#include "dx12/dx12_pipeline_state.hpp"
#include "dx12/dx12_buffer.hpp"
#include "dx12/dx12_staging_buffer.hpp"
#endif

#include "window.hpp"