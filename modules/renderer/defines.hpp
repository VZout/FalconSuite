// Interface Handles
#define DEC_HANDLE(name) typedef struct _ ## name name;

// Result Check
#ifdef FR_USE_VULKAN
#define CHECK_API_RESULT(r) 																			\
{																										\
    VkResult res = (r);																					\
    if (res != VK_SUCCESS)																				\
    {																									\
        printf("Fatal : VkResult is %d in %s at line %d\n", res,  __FILE__, __LINE__);					\
        assert(res == VK_SUCCESS);																		\
    }																									\
}
//#elif FR_USE_DX12
//#define CHECK_API_RESULT(r) /* ... */
#else
#define CHECK_API_RESULT(r)
#endif