#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set=0, binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
	vec3 lightPos;
	float time;
} ubo;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec3 fragPos;
layout(location = 2) out vec3 outNormal;
layout(location = 3) out vec3 outLightVec;
layout(location = 4) out vec2 fragTexCoord;
layout(location = 5) out float outTime;

void main() {
	fragColor = vec3(1,0,0);
	fragTexCoord = inTexCoord;
	outTime = ubo.time;

	mat4 modelView = ubo.view * ubo.model;

	outNormal = mat3(transpose(inverse(ubo.model))) * inNormal;
	outLightVec = ubo.lightPos;

	// gl position
	gl_Position = ubo.proj * modelView * vec4(inPosition, 1.0);
}
