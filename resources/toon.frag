#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec3 fragPos;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec3 inLightVec;
layout(location = 4) in vec2 inFragTexCoord;
layout(location = 5) in float inTime;

layout(set=1, binding = 0) uniform sampler2D texSampler[3];

layout(location = 0) out vec4 outColor;

void main() {
	vec3 albado = texture(texSampler[0], inFragTexCoord).rgb;
	vec3 lightColor = vec3(1, 1, 1);

	vec3 norm = normalize(inNormal);

    vec3 lightDir = normalize(inLightVec - fragPos);
    float diff = max(dot(norm, lightDir), 0.0);

	// Toon hardcoded in shader
	if (diff > 0.45)
		lightColor *= 1.0;
	else if (diff > 0.2)
		lightColor *= 0.4;
	else
		lightColor *= 0;

	// Standard diffuse
	vec3 diffuse = diff * lightColor;

	// Toon from texture (texture needs to be clamed to edge)
	//float ndotl = dot(norm, inLightVec);
	//vec3 lut = texture(texSampler[1], vec2(ndotl, 0)).rgb;	
	//diffuse = lut * lightColor;

	vec3 result = diffuse * albado;
    outColor = vec4(result, 1.0);
}

