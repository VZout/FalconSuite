#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec3 fragPos;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec3 inLightVec;
layout(location = 4) in vec2 inFragTexCoord;
layout(location = 5) in float inTime;

layout(set=1, binding = 0) uniform sampler2D texSampler[3];

layout(location = 0) out vec4 outColor;

vec3 Freshnel(in vec3 SpecularColor, in vec3 PixelNormal, in vec3 LightDir, in float strength) {
    float NdotL = max(0, dot(PixelNormal, LightDir));
    return SpecularColor * pow(( 1 - NdotL ), strength);
}

vec3 SpecFreshnel(in vec3 SpecularColor, in vec3 PixelNormal, in vec3 LightDir, in float strength) {
    float NdotL = max(0, dot(PixelNormal, LightDir));
    return SpecularColor + (1 - SpecularColor) * pow(( 1 - NdotL ), strength);
}

void main() {
	vec3 clouds = texture(texSampler[1], vec2(inFragTexCoord.x + inTime, inFragTexCoord.y)).rgb;
	vec3 albado = texture(texSampler[0], inFragTexCoord).rgb + clouds;
	vec3 lightColor = vec3(1, 1, 1);

	vec3 norm = normalize(inNormal);
    vec3 lightDir = normalize(inLightVec - fragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;

	vec3 fresh = Freshnel(vec3(0, 0.8, 1), inNormal, vec3(0,0,-1), 2);
	fresh *= diffuse;

	vec3 night = texture(texSampler[2], inFragTexCoord).rgb;
	vec3 night_color = vec3(1, 0.6f, 0);
	night *= night_color;
	//night = normalize(night);
	night *= 1 - (clouds);
	night *= 1 - (diffuse);
	night *= 0.75;

	vec3 result = (diffuse) * albado;
    outColor = vec4(result + night + fresh, 1.0);
}
