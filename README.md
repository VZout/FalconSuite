# [FalconSuite](#) - Cross-platform game development suite

![License](https://img.shields.io/badge/license-BSD--2%20clause-blue.svg)

## Modules
- Falcon<strong>Renderer</strong> - *Low level graphics API acnostic library.* (WIP)
- Falcon<strong>Math</strong> - *Matrix and Vector math.* (WIP)
- Falcon<strong>Utils</strong> - *coroutines, delegates and etc.* (Coming soon)
- Falcon<strong>Engine</strong> - *2D / 3D game engine.* (Coming soon)

## [Falcon<strong>Renderer</strong>](#)

FalconRenderer is a low level graphics API acnostic libraray.

### Goals
My goal is to create a easy to use, low level graphics library that is similairly designed as DirectX 12 and Vulkan to allow developers to quickly learn and use the API. I also aim to design the library with c++14 and object oriented programming in mind.

I wan't the library to take advantage off vulkan and DX12. Other API's are secondary and may objectively be worse when using this library. This allows application to run better on the biggest platforms and newer machines.

* Light Weight
* Low Level
* True to the underlying graphics API's
* Little Dependencies
* C++ 14 / 17

### Rendering Backends

API                    |Progress
-----------------------|---------
 DirectX | 0%
 Vulkan         | 50%
 GNM                   | 0%

### Platforms

Platform        |API                    |Priority
----------------|-----------------------|---------
Windows 7, 8, 10| DirectX 12 and Vulkan | High
Linux           | Vulkan | High         | High
Playstation 4   | GNM                   | High
XBox One        | DirectX 12            | Medium
Windows Phone   | DirectX 12            | Medium
Android         | Vulkan                | Low

### Compilers

* Clang 3.3 and above (No DirectX)
* GCC 4.6 and above (No DirectX)
* VS2012 and above

## [Falcon<strong>Math</strong>](#)

Vector and Matrix mathmatical operations.

## [License (BSD 2-clause)](#)

